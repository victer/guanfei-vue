import request from '@/utils/request'

// 查询人员证件信息列表
export function listCard(query) {
  return request({
    url: '/guanfei/basic/card/list',
    method: 'get',
    params: query
  })
}

// 查询人员证件信息详细
export function getCard(id) {
  return request({
    url: '/guanfei/basic/card/getOne/' + id,
    method: 'get'
  })
}

// 新增人员证件信息
export function addCard(data) {
  return request({
    url: '/guanfei/basic/card/add',
    method: 'post',
    data: data
  })
}

// 修改人员证件信息
export function updateCard(data) {
  return request({
    url: '/guanfei/basic/card/update',
    method: 'put',
    data: data
  })
}

// 删除人员证件信息
export function delCard(id) {
  return request({
    url: '/guanfei/basic/card/delete/' + id,
    method: 'delete'
  })
}

// 导出人员证件信息
export function exportCard(query) {
  return request({
    url: '/guanfei/basic/card/export',
    method: 'get',
    params: query
  })
}
