import request from '@/utils/request'

// 查询合同模板信息列表
export function listTemplate(query) {
  return request({
    url: '/guanfei/basic/template/list',
    method: 'get',
    params: query
  })
}

// 查询合同模板信息详细
export function getTemplate(id) {
  return request({
    url: '/guanfei/basic/template/getOne/' + id,
    method: 'get'
  })
}

// 新增合同模板信息
export function addTemplate(data) {
  return request({
    url: '/guanfei/basic/template/add',
    method: 'post',
    data: data
  })
}

// 修改合同模板信息
export function updateTemplate(data) {
  return request({
    url: '/guanfei/basic/template/update',
    method: 'put',
    data: data
  })
}

// 删除合同模板信息
export function delTemplate(id) {
  return request({
    url: '/guanfei/basic/template/delete/' + id,
    method: 'delete'
  })
}

// 导出合同模板信息
export function exportTemplate(query) {
  return request({
    url: '/guanfei/basic/template/export',
    method: 'get',
    params: query
  })
}
