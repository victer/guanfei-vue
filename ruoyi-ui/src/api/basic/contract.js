import request from '@/utils/request'

// 查询合同信息列表
export function listContract(query) {
  return request({
    url: '/guanfei/basic/contract/list',
    method: 'get',
    params: query
  })
}

// 查询合同信息详细
export function getContract(id) {
  return request({
    url: '/guanfei/basic/contract/getOne/' + id,
    method: 'get'
  })
}

// 新增合同信息
export function addContract(data) {
  return request({
    url: '/guanfei/basic/contract/add',
    method: 'post',
    data: data
  })
}

// 修改合同信息
export function updateContract(data) {
  return request({
    url: '/guanfei/basic/contract/update',
    method: 'put',
    data: data
  })
}

// 删除合同信息
export function delContract(id) {
  return request({
    url: '/guanfei/basic/contract/delete/' + id,
    method: 'delete'
  })
}

// 导出合同信息
export function exportContract(query) {
  return request({
    url: '/guanfei/basic/contract/export',
    method: 'get',
    params: query
  })
}

//查询田块信息
export function getFieldList() {
  return request({
    url: '/guanfei/basic/field/listNoPaging',
    method: 'get'
  })
}

//查询合同模板信息
export function getTemplateList() {
  return request({
    url: '/guanfei/basic/template/listNoPaging',
    method: 'get'
  })
}

//查询人员信息
export function getPeopleList() {
  return request({
    url: 'guanfei/basic/people/listNoPaging',
    method: 'get'
  })
}
