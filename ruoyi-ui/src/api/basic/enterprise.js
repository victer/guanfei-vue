import request from '@/utils/request'

// 查询农资企业信息列表
export function listEnterprise(query) {
  return request({
    url: '/guanfei/basic/enterprise/list',
    method: 'get',
    params: query
  })
}

// 查询农资企业信息详细
export function getEnterprise(id) {
  return request({
    url: '/guanfei/basic/enterprise/getOne/' + id,
    method: 'get'
  })
}

// 新增农资企业信息
export function addEnterprise(data) {
  return request({
    url: '/guanfei/basic/enterprise/add',
    method: 'post',
    data: data
  })
}

// 修改农资企业信息
export function updateEnterprise(data) {
  return request({
    url: '/guanfei/basic/enterprise/update',
    method: 'put',
    data: data
  })
}

// 删除农资企业信息
export function delEnterprise(id) {
  return request({
    url: '/guanfei/basic/enterprise/delete/' + id,
    method: 'delete'
  })
}

// 导出农资企业信息
export function exportEnterprise(query) {
  return request({
    url: '/guanfei/basic/enterprise/export',
    method: 'get',
    params: query
  })
}