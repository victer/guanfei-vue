import request from '@/utils/request'

// 查询田块信息列表
export function listField(query) {
  return request({
    url: '/guanfei/basic/field/list',
    method: 'get',
    params: query
  })
}

// 查询田块信息详细
export function getField(id) {
  return request({
    url: '/guanfei/basic/field/getOne/' + id,
    method: 'get'
  })
}

// 新增田块信息
export function addField(data) {
  return request({
    url: '/guanfei/basic/field/add',
    method: 'post',
    data: data
  })
}

// 修改田块信息
export function updateField(data) {
  return request({
    url: '/guanfei/basic/field/update',
    method: 'put',
    data: data
  })
}

// 删除田块信息
export function delField(id) {
  return request({
    url: '/guanfei/basic/field/delete/' + id,
    method: 'delete'
  })
}

// 导出田块信息
export function exportField(query) {
  return request({
    url: '/guanfei/basic/field/export',
    method: 'get',
    params: query
  })
}