import request from '@/utils/request'

// 查询资讯信息列表
export function listArticle(query) {
  return request({
    url: '/guanfei/basic/article/list',
    method: 'get',
    params: query
  })
}

// 查询资讯信息详细
export function getArticle(id) {
  return request({
    url: '/guanfei/basic/article/' + id,
    method: 'get'
  })
}

// 新增资讯信息
export function addArticle(data) {
  return request({
    url: '/guanfei/basic/article/add',
    method: 'post',
    data: data
  })
}

// 修改资讯信息
export function updateArticle(data) {
  return request({
    url: '/guanfei/basic/article/edit',
    method: 'put',
    data: data
  })
}

// 删除资讯信息
export function delArticle(id) {
  return request({
    url: '/guanfei/basic/article/' + id,
    method: 'delete'
  })
}

// 导出资讯信息
export function exportArticle(query) {
  return request({
    url: '/guanfei/basic/article/export',
    method: 'get',
    params: query
  })
}

// 获取资讯分类信息
export function getArticleCategoryList() {
  return request({
    url: '/guanfei/basic/category/listNoPage',
    method: 'get'
  })
}

// 获取农资产品信息
export function getGoodsList() {
  return request({
    url: '/guanfei/basic/goods/listNoPage',
    method: 'get'
  })
}