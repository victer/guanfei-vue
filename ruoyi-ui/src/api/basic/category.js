import request from '@/utils/request'

// 查询资讯信息分类列表
export function listCategory(query) {
  return request({
    url: '/guanfei/basic/category/list',
    method: 'get',
    params: query
  })
}

// 查询资讯信息分类详细
export function getCategory(id) {
  return request({
    url: '/guanfei/basic/category/' + id,
    method: 'get'
  })
}

// 新增资讯信息分类
export function addCategory(data) {
  return request({
    url: '/guanfei/basic/category/add',
    method: 'post',
    data: data
  })
}

// 修改资讯信息分类
export function updateCategory(data) {
  return request({
    url: '/guanfei/basic/category/edit',
    method: 'put',
    data: data
  })
}

// 删除资讯信息分类
export function delCategory(id) {
  return request({
    url: '/guanfei/basic/category/' + id,
    method: 'delete'
  })
}

// 导出资讯信息分类
export function exportCategory(query) {
  return request({
    url: '/guanfei/basic/category/export',
    method: 'get',
    params: query
  })
}
