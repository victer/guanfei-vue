import request from '@/utils/request'

// 查询人员信息列表
export function listPeople(query) {
  return request({
    url: '/guanfei/basic/people/list',
    method: 'get',
    params: query
  })
}

// 查询人员信息详细
export function getPeople(id) {
  return request({
    url: '/guanfei/basic/people/getOne/' + id,
    method: 'get'
  })
}

// 新增人员信息
export function addPeople(data) {
  return request({
    url: '/guanfei/basic/people/add',
    method: 'post',
    data: data
  })
}

// 修改人员信息
export function updatePeople(data) {
  return request({
    url: '/guanfei/basic/people/update',
    method: 'put',
    data: data
  })
}

// 删除人员信息
export function delPeople(id) {
  return request({
    url: '/guanfei/basic/people/delete/' + id,
    method: 'delete'
  })
}

// 导出人员信息
export function exportPeople(query) {
  return request({
    url: '/guanfei/basic/people/export',
    method: 'get',
    params: query
  })
}