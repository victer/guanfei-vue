import request from '@/utils/request'

// 查询农机信息列表
export function listFarm(query) {
  return request({
    url: '/guanfei/basic/farm/list',
    method: 'get',
    params: query
  })
}

// 查询农机信息详细
export function getFarm(id) {
  return request({
    url: '/guanfei/basic/farm/getOne/' + id,
    method: 'get'
  })
}

// 新增农机信息
export function addFarm(data) {
  return request({
    url: '/guanfei/basic/farm/add',
    method: 'post',
    data: data
  })
}

// 修改农机信息
export function updateFarm(data) {
  return request({
    url: '/guanfei/basic/farm/update',
    method: 'put',
    data: data
  })
}

// 删除农机信息
export function delFarm(id) {
  return request({
    url: '/guanfei/basic/farm/delete/' + id,
    method: 'delete'
  })
}

// 导出农机信息
export function exportFarm(query) {
  return request({
    url: '/guanfei/basic/farm/export',
    method: 'get',
    params: query
  })
}

//获取挂靠组织名称
export function getFarmDept(deptId) {
  return request({
    url: '/system/dept/'+deptId,
    method: 'get'
  })
}

//查询所有人员数据
export function getFarmPeopleList() {
  return request({
    url: 'guanfei/basic/people/listNoPaging',
    method: 'get'
  })
}
