import request from '@/utils/request'

// 查询农资产品信息列表
export function listGoods(query) {
  return request({
    url: '/guanfei/basic/goods/list',
    method: 'get',
    params: query
  })
}

// 查询农资产品信息详细
export function getGoods(id) {
  return request({
    url: '/guanfei/basic/goods/getOne/' + id,
    method: 'get'
  })
}

// 新增农资产品信息
export function addGoods(data) {
  return request({
    url: '/guanfei/basic/goods/add',
    method: 'post',
    data: data
  })
}

// 修改农资产品信息
export function updateGoods(data) {
  return request({
    url: '/guanfei/basic/goods/update',
    method: 'put',
    data: data
  })
}

// 删除农资产品信息
export function delGoods(id) {
  return request({
    url: '/guanfei/basic/goods/delete' + id,
    method: 'delete'
  })
}

// 导出农资产品信息
export function exportGoods(query) {
  return request({
    url: '/guanfei/basic/goods/export',
    method: 'get',
    params: query
  })
}