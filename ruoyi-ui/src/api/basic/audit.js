import request from '@/utils/request'

// 查询农资企业认证信息列表
export function listAudit(query) {
  return request({
    url: '/guanfei/basic/audit/list',
    method: 'get',
    params: query
  })
}

// 查询农资企业认证信息详细
export function getAudit(id) {
  return request({
    url: '/guanfei/basic/audit/getOne/' + id,
    method: 'get'
  })
}

// 新增农资企业认证信息
export function addAudit(data) {
  return request({
    url: '/guanfei/basic/audit/add',
    method: 'post',
    data: data
  })
}

// 修改农资企业认证信息
export function updateAudit(data) {
  return request({
    url: '/guanfei/basic/audit/update',
    method: 'put',
    data: data
  })
}

// 删除农资企业认证信息
export function delAudit(id) {
  return request({
    url: '/guanfei/basic/audit/delete/' + id,
    method: 'delete'
  })
}

// 导出农资企业认证信息
export function exportAudit(query) {
  return request({
    url: '/guanfei/basic/audit/export',
    method: 'get',
    params: query
  })
}

// 查询农资企业信息
export function getEnterpriseList() {
  return request({
    url: '/guanfei/basic/enterprise/listNoPage',
    method: 'get'
  })
}
