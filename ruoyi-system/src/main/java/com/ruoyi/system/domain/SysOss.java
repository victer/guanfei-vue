package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件上传对象 sys_oss
 * 
 * @author ace
 * @date 2021-06-23
 */
public class SysOss extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 新文件名 */
    @Excel(name = "文件名")
    private String newFileName;

    /** 文件后缀名 */
    @Excel(name = "文件后缀名")
    private String fileSuffix;

    /** URL地址 */
    @Excel(name = "URL地址")
    private String url;

    /** 服务商 */
    @Excel(name = "服务商")
    private Integer service;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFileSuffix(String fileSuffix) 
    {
        this.fileSuffix = fileSuffix;
    }

    public String getFileSuffix() 
    {
        return fileSuffix;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setService(Integer service) 
    {
        this.service = service;
    }

    public Integer getService() 
    {
        return service;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("fileName", getFileName())
            .append("fileSuffix", getFileSuffix())
            .append("url", getUrl())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("service", getService())
            .append("newFileName", getNewFileName())
            .toString();
    }
}
