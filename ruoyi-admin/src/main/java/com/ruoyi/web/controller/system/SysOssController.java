package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysOss;
import com.ruoyi.system.service.ISysOssService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件上传Controller
 * 
 * @author ace
 * @date 2021-06-23
 */
@RestController
@RequestMapping("/system/oss")
public class SysOssController extends BaseController
{
    @Autowired
    private ISysOssService sysOssService;

    /**
     * 查询文件上传列表
     */
    @PreAuthorize("@ss.hasPermi('system:oss:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysOss sysOss)
    {
        startPage();
        List<SysOss> list = sysOssService.selectSysOssList(sysOss);
        return getDataTable(list);
    }

    /**
     * 导出文件上传列表
     */
    @PreAuthorize("@ss.hasPermi('system:oss:export')")
    @Log(title = "文件上传", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysOss sysOss)
    {
        List<SysOss> list = sysOssService.selectSysOssList(sysOss);
        ExcelUtil<SysOss> util = new ExcelUtil<SysOss>(SysOss.class);
        return util.exportExcel(list, "文件上传数据");
    }

    /**
     * 获取文件上传详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:oss:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysOssService.selectSysOssById(id));
    }

    /**
     * 新增文件上传
     */
    @PreAuthorize("@ss.hasPermi('system:oss:add')")
    @Log(title = "文件上传", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysOss sysOss)
    {
        return toAjax(sysOssService.insertSysOss(sysOss));
    }

    /**
     * 修改文件上传
     */
    @PreAuthorize("@ss.hasPermi('system:oss:edit')")
    @Log(title = "文件上传", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysOss sysOss)
    {
        return toAjax(sysOssService.updateSysOss(sysOss));
    }

    /**
     * 删除文件上传
     */
    @PreAuthorize("@ss.hasPermi('system:oss:remove')")
    @Log(title = "文件上传", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysOssService.deleteSysOssByIds(ids));
    }
}
