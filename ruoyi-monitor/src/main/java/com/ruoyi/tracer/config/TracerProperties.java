package com.ruoyi.tracer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * BizTracer配置类
 *
 * @author ace
 */
@ConfigurationProperties("yudao.tracer")
@Data
public class TracerProperties {
}
