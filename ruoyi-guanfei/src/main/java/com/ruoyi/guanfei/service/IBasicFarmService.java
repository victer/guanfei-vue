package com.ruoyi.guanfei.service;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicFarm;

/**
 * 农机信息Service接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface IBasicFarmService 
{
    /**
     * 查询农机信息
     * 
     * @param id 农机信息ID
     * @return 农机信息
     */
    public BasicFarm selectBasicFarmById(Long id);

    /**
     * 查询农机信息列表
     * 
     * @param basicFarm 农机信息
     * @return 农机信息集合
     */
    public List<BasicFarm> selectBasicFarmList(BasicFarm basicFarm);

    /**
     * 新增农机信息
     * 
     * @param basicFarm 农机信息
     * @return 结果
     */
    public int insertBasicFarm(BasicFarm basicFarm);

    /**
     * 修改农机信息
     * 
     * @param basicFarm 农机信息
     * @return 结果
     */
    public int updateBasicFarm(BasicFarm basicFarm);

    /**
     * 批量删除农机信息
     * 
     * @param ids 需要删除的农机信息ID
     * @return 结果
     */
    public int deleteBasicFarmByIds(Long[] ids);

    /**
     * 删除农机信息信息
     * 
     * @param id 农机信息ID
     * @return 结果
     */
    public int deleteBasicFarmById(Long id);
}
