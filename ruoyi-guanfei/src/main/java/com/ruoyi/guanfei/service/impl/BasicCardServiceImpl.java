package com.ruoyi.guanfei.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicCardMapper;
import com.ruoyi.guanfei.domain.BasicCard;
import com.ruoyi.guanfei.service.IBasicCardService;

/**
 * 人员证件信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Service
public class BasicCardServiceImpl implements IBasicCardService 
{
    @Autowired
    private BasicCardMapper basicCardMapper;

    /**
     * 查询人员证件信息
     * 
     * @param id 人员证件信息ID
     * @return 人员证件信息
     */
    @Override
    public BasicCard selectBasicCardById(Long id)
    {
        return basicCardMapper.selectBasicCardById(id);
    }

    /**
     * 查询人员证件信息列表
     * 
     * @param basicCard 人员证件信息
     * @return 人员证件信息
     */
    @Override
    public List<BasicCard> selectBasicCardList(BasicCard basicCard)
    {
        return basicCardMapper.selectBasicCardList(basicCard);
    }

    /**
     * 新增人员证件信息
     * 
     * @param basicCard 人员证件信息
     * @return 结果
     */
    @Override
    public int insertBasicCard(BasicCard basicCard)
    {
        return basicCardMapper.insertBasicCard(basicCard);
    }

    /**
     * 修改人员证件信息
     * 
     * @param basicCard 人员证件信息
     * @return 结果
     */
    @Override
    public int updateBasicCard(BasicCard basicCard)
    {
        return basicCardMapper.updateBasicCard(basicCard);
    }

    /**
     * 批量删除人员证件信息
     * 
     * @param ids 需要删除的人员证件信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicCardByIds(Long[] ids)
    {
        return basicCardMapper.deleteBasicCardByIds(ids);
    }

    /**
     * 删除人员证件信息信息
     * 
     * @param id 人员证件信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicCardById(Long id)
    {
        return basicCardMapper.deleteBasicCardById(id);
    }
}
