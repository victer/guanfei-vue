package com.ruoyi.guanfei.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicContractTemplateMapper;
import com.ruoyi.guanfei.domain.BasicContractTemplate;
import com.ruoyi.guanfei.service.IBasicContractTemplateService;

/**
 * 合同模板信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Service
public class BasicContractTemplateServiceImpl implements IBasicContractTemplateService 
{
    @Autowired
    private BasicContractTemplateMapper basicContractTemplateMapper;

    /**
     * 查询合同模板信息
     * 
     * @param id 合同模板信息ID
     * @return 合同模板信息
     */
    @Override
    public BasicContractTemplate selectBasicContractTemplateById(Long id)
    {
        return basicContractTemplateMapper.selectBasicContractTemplateById(id);
    }

    /**
     * 查询合同模板信息列表
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 合同模板信息
     */
    @Override
    public List<BasicContractTemplate> selectBasicContractTemplateList(BasicContractTemplate basicContractTemplate)
    {
        return basicContractTemplateMapper.selectBasicContractTemplateList(basicContractTemplate);
    }

    /**
     * 新增合同模板信息
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 结果
     */
    @Override
    public int insertBasicContractTemplate(BasicContractTemplate basicContractTemplate)
    {
        return basicContractTemplateMapper.insertBasicContractTemplate(basicContractTemplate);
    }

    /**
     * 修改合同模板信息
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 结果
     */
    @Override
    public int updateBasicContractTemplate(BasicContractTemplate basicContractTemplate)
    {
        return basicContractTemplateMapper.updateBasicContractTemplate(basicContractTemplate);
    }

    /**
     * 批量删除合同模板信息
     * 
     * @param ids 需要删除的合同模板信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractTemplateByIds(Long[] ids)
    {
        return basicContractTemplateMapper.deleteBasicContractTemplateByIds(ids);
    }

    /**
     * 删除合同模板信息信息
     * 
     * @param id 合同模板信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractTemplateById(Long id)
    {
        return basicContractTemplateMapper.deleteBasicContractTemplateById(id);
    }

    /**
     * 查询合同模板信息（不分页）
     * @param
     * @return
     */
    @Override
    public List<BasicContractTemplate> listNoPaging() {
        return basicContractTemplateMapper.selectBasicContractTemplateList(null);
    }
}
