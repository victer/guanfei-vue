package com.ruoyi.guanfei.service;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicEnterprise;

/**
 * 农资企业信息Service接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface IBasicEnterpriseService 
{
    /**
     * 查询农资企业信息
     * 
     * @param id 农资企业信息ID
     * @return 农资企业信息
     */
    public BasicEnterprise selectBasicEnterpriseById(Long id);

    /**
     * 查询农资企业信息列表
     * 
     * @param basicEnterprise 农资企业信息
     * @return 农资企业信息集合
     */
    public List<BasicEnterprise> selectBasicEnterpriseList(BasicEnterprise basicEnterprise);

    /**
     * 新增农资企业信息
     * 
     * @param basicEnterprise 农资企业信息
     * @return 结果
     */
    public int insertBasicEnterprise(BasicEnterprise basicEnterprise);

    /**
     * 修改农资企业信息
     * 
     * @param basicEnterprise 农资企业信息
     * @return 结果
     */
    public int updateBasicEnterprise(BasicEnterprise basicEnterprise);

    /**
     * 批量删除农资企业信息
     * 
     * @param ids 需要删除的农资企业信息ID
     * @return 结果
     */
    public int deleteBasicEnterpriseByIds(Long[] ids);

    /**
     * 删除农资企业信息信息
     * 
     * @param id 农资企业信息ID
     * @return 结果
     */
    public int deleteBasicEnterpriseById(Long id);
}
