package com.ruoyi.guanfei.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicGoodsMapper;
import com.ruoyi.guanfei.domain.BasicGoods;
import com.ruoyi.guanfei.service.IBasicGoodsService;

/**
 * 农资产品信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Service
public class BasicGoodsServiceImpl implements IBasicGoodsService 
{
    @Autowired
    private BasicGoodsMapper basicGoodsMapper;

    /**
     * 查询农资产品信息
     * 
     * @param id 农资产品信息ID
     * @return 农资产品信息
     */
    @Override
    public BasicGoods selectBasicGoodsById(Long id)
    {
        return basicGoodsMapper.selectBasicGoodsById(id);
    }

    /**
     * 查询农资产品信息列表
     * 
     * @param basicGoods 农资产品信息
     * @return 农资产品信息
     */
    @Override
    public List<BasicGoods> selectBasicGoodsList(BasicGoods basicGoods)
    {
        return basicGoodsMapper.selectBasicGoodsList(basicGoods);
    }

    /**
     * 新增农资产品信息
     * 
     * @param basicGoods 农资产品信息
     * @return 结果
     */
    @Override
    public int insertBasicGoods(BasicGoods basicGoods)
    {
        return basicGoodsMapper.insertBasicGoods(basicGoods);
    }

    /**
     * 修改农资产品信息
     * 
     * @param basicGoods 农资产品信息
     * @return 结果
     */
    @Override
    public int updateBasicGoods(BasicGoods basicGoods)
    {
        return basicGoodsMapper.updateBasicGoods(basicGoods);
    }

    /**
     * 批量删除农资产品信息
     * 
     * @param ids 需要删除的农资产品信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicGoodsByIds(Long[] ids)
    {
        return basicGoodsMapper.deleteBasicGoodsByIds(ids);
    }

    /**
     * 删除农资产品信息信息
     * 
     * @param id 农资产品信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicGoodsById(Long id)
    {
        return basicGoodsMapper.deleteBasicGoodsById(id);
    }
}
