package com.ruoyi.guanfei.service;

import com.ruoyi.guanfei.domain.ShopCouponUser;

import java.util.List;

/**
 * 优惠券发放记录Service接口
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
public interface IShopCouponUserService 
{
    /**
     * 查询优惠券发放记录
     * 
     * @param id 优惠券发放记录ID
     * @return 优惠券发放记录
     */
    public ShopCouponUser selectShopCouponUserById(Long id);

    /**
     * 查询优惠券发放记录列表
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 优惠券发放记录集合
     */
    public List<ShopCouponUser> selectShopCouponUserList(ShopCouponUser shopCouponUser);

    /**
     * 新增优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    public int insertShopCouponUser(ShopCouponUser shopCouponUser);

    /**
     * 修改优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    public int updateShopCouponUser(ShopCouponUser shopCouponUser);

    /**
     * 批量删除优惠券发放记录
     * 
     * @param ids 需要删除的优惠券发放记录ID
     * @return 结果
     */
    public int deleteShopCouponUserByIds(Long[] ids);

    /**
     * 删除优惠券发放记录信息
     * 
     * @param id 优惠券发放记录ID
     * @return 结果
     */
    public int deleteShopCouponUserById(Long id);
}
