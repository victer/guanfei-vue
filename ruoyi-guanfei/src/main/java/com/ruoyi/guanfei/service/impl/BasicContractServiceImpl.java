package com.ruoyi.guanfei.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicContractMapper;
import com.ruoyi.guanfei.domain.BasicContract;
import com.ruoyi.guanfei.service.IBasicContractService;

/**
 * 合同信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Service
public class BasicContractServiceImpl implements IBasicContractService 
{
    @Autowired
    private BasicContractMapper basicContractMapper;

    /**
     * 查询合同信息
     * 
     * @param id 合同信息ID
     * @return 合同信息
     */
    @Override
    public BasicContract selectBasicContractById(Long id)
    {
        return basicContractMapper.selectBasicContractById(id);
    }

    /**
     * 查询合同信息列表
     * 
     * @param basicContract 合同信息
     * @return 合同信息
     */
    @Override
    public List<BasicContract> selectBasicContractList(BasicContract basicContract)
    {
        return basicContractMapper.selectBasicContractList(basicContract);
    }

    /**
     * 新增合同信息
     * 
     * @param basicContract 合同信息
     * @return 结果
     */
    @Override
    public int insertBasicContract(BasicContract basicContract)
    {
        basicContract.setCreateTime(DateUtils.getNowDate());
        return basicContractMapper.insertBasicContract(basicContract);
    }

    /**
     * 修改合同信息
     * 
     * @param basicContract 合同信息
     * @return 结果
     */
    @Override
    public int updateBasicContract(BasicContract basicContract)
    {
        basicContract.setUpdateTime(DateUtils.getNowDate());
        return basicContractMapper.updateBasicContract(basicContract);
    }

    /**
     * 批量删除合同信息
     * 
     * @param ids 需要删除的合同信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractByIds(Long[] ids)
    {
        return basicContractMapper.deleteBasicContractByIds(ids);
    }

    /**
     * 删除合同信息信息
     * 
     * @param id 合同信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractById(Long id)
    {
        return basicContractMapper.deleteBasicContractById(id);
    }
}
