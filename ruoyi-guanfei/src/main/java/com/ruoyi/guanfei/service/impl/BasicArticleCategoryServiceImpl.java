package com.ruoyi.guanfei.service.impl;

import com.ruoyi.guanfei.domain.BasicArticleCategory;
import com.ruoyi.guanfei.mapper.BasicArticleCategoryMapper;
import com.ruoyi.guanfei.service.IBasicArticleCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资讯信息分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Service
public class BasicArticleCategoryServiceImpl implements IBasicArticleCategoryService
{
    @Autowired
    private BasicArticleCategoryMapper basicArticleCategoryMapper;

    /**
     * 查询资讯信息分类
     * 
     * @param id 资讯信息分类ID
     * @return 资讯信息分类
     */
    @Override
    public BasicArticleCategory selectBasicArticleCategoryById(Long id)
    {
        return basicArticleCategoryMapper.selectBasicArticleCategoryById(id);
    }

    /**
     * 查询资讯信息分类列表
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 资讯信息分类
     */
    @Override
    public List<BasicArticleCategory> selectBasicArticleCategoryList(BasicArticleCategory basicArticleCategory)
    {
        return basicArticleCategoryMapper.selectBasicArticleCategoryList(basicArticleCategory);
    }

    /**
     * 新增资讯信息分类
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 结果
     */
    @Override
    public int insertBasicArticleCategory(BasicArticleCategory basicArticleCategory)
    {
        return basicArticleCategoryMapper.insertBasicArticleCategory(basicArticleCategory);
    }

    /**
     * 修改资讯信息分类
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 结果
     */
    @Override
    public int updateBasicArticleCategory(BasicArticleCategory basicArticleCategory)
    {
        return basicArticleCategoryMapper.updateBasicArticleCategory(basicArticleCategory);
    }

    /**
     * 批量删除资讯信息分类
     * 
     * @param ids 需要删除的资讯信息分类ID
     * @return 结果
     */
    @Override
    public int deleteBasicArticleCategoryByIds(Long[] ids)
    {
        return basicArticleCategoryMapper.deleteBasicArticleCategoryByIds(ids);
    }

    /**
     * 删除资讯信息分类信息
     * 
     * @param id 资讯信息分类ID
     * @return 结果
     */
    @Override
    public int deleteBasicArticleCategoryById(Long id)
    {
        return basicArticleCategoryMapper.deleteBasicArticleCategoryById(id);
    }
}
