package com.ruoyi.guanfei.service;

import java.util.List;

import com.ruoyi.guanfei.domain.BasicPeople;

/**
 * 人员信息Service接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface IBasicPeopleService
{
    /**
     * 查询人员信息
     * 
     * @param id 人员信息ID
     * @return 人员信息
     */
    public BasicPeople selectBasicPeopleById(Long id);

    /**
     * 查询人员信息列表
     * 
     * @param basicPeople 人员信息
     * @return 人员信息集合
     */
    public List<BasicPeople> selectBasicPeopleList(BasicPeople basicPeople);

    /**
     * 新增人员信息
     * 
     * @param basicPeople 人员信息
     * @return 结果
     */
    public int insertBasicPeople(BasicPeople basicPeople);

    /**
     * 修改人员信息
     * 
     * @param basicPeople 人员信息
     * @return 结果
     */
    public int updateBasicPeople(BasicPeople basicPeople);

    /**
     * 批量删除人员信息
     * 
     * @param ids 需要删除的人员信息ID
     * @return 结果
     */
    public int deleteBasicPeopleByIds(Long[] ids);

    /**
     * 删除人员信息信息
     * 
     * @param id 人员信息ID
     * @return 结果
     */
    public int deleteBasicPeopleById(Long id);

    /**
     * 查询人员信息列表(不分页)
     * @param
     * @return 结果
     */
    public List<BasicPeople> listNoPaging();
}
