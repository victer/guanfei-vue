package com.ruoyi.guanfei.service.impl;

import com.ruoyi.guanfei.domain.ShopCouponIssueUser;
import com.ruoyi.guanfei.mapper.ShopCouponIssueUserMapper;
import com.ruoyi.guanfei.service.IShopCouponIssueUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 优惠券领取Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Service
public class ShopCouponIssueUserServiceImpl implements IShopCouponIssueUserService
{
    @Autowired
    private ShopCouponIssueUserMapper shopCouponIssueUserMapper;

    /**
     * 查询优惠券领取
     * 
     * @param id 优惠券领取ID
     * @return 优惠券领取
     */
    @Override
    public ShopCouponIssueUser selectShopCouponIssueUserById(Long id)
    {
        return shopCouponIssueUserMapper.selectShopCouponIssueUserById(id);
    }

    /**
     * 查询优惠券领取列表
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 优惠券领取
     */
    @Override
    public List<ShopCouponIssueUser> selectShopCouponIssueUserList(ShopCouponIssueUser shopCouponIssueUser)
    {
        return shopCouponIssueUserMapper.selectShopCouponIssueUserList(shopCouponIssueUser);
    }

    /**
     * 新增优惠券领取
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 结果
     */
    @Override
    public int insertShopCouponIssueUser(ShopCouponIssueUser shopCouponIssueUser)
    {
        return shopCouponIssueUserMapper.insertShopCouponIssueUser(shopCouponIssueUser);
    }

    /**
     * 修改优惠券领取
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 结果
     */
    @Override
    public int updateShopCouponIssueUser(ShopCouponIssueUser shopCouponIssueUser)
    {
        return shopCouponIssueUserMapper.updateShopCouponIssueUser(shopCouponIssueUser);
    }

    /**
     * 批量删除优惠券领取
     * 
     * @param ids 需要删除的优惠券领取ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponIssueUserByIds(Long[] ids)
    {
        return shopCouponIssueUserMapper.deleteShopCouponIssueUserByIds(ids);
    }

    /**
     * 删除优惠券领取信息
     * 
     * @param id 优惠券领取ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponIssueUserById(Long id)
    {
        return shopCouponIssueUserMapper.deleteShopCouponIssueUserById(id);
    }
}
