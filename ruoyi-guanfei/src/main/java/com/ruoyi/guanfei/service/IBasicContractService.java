package com.ruoyi.guanfei.service;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicContract;

/**
 * 合同信息Service接口
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
public interface IBasicContractService 
{
    /**
     * 查询合同信息
     * 
     * @param id 合同信息ID
     * @return 合同信息
     */
    public BasicContract selectBasicContractById(Long id);

    /**
     * 查询合同信息列表
     * 
     * @param basicContract 合同信息
     * @return 合同信息集合
     */
    public List<BasicContract> selectBasicContractList(BasicContract basicContract);

    /**
     * 新增合同信息
     * 
     * @param basicContract 合同信息
     * @return 结果
     */
    public int insertBasicContract(BasicContract basicContract);

    /**
     * 修改合同信息
     * 
     * @param basicContract 合同信息
     * @return 结果
     */
    public int updateBasicContract(BasicContract basicContract);

    /**
     * 批量删除合同信息
     * 
     * @param ids 需要删除的合同信息ID
     * @return 结果
     */
    public int deleteBasicContractByIds(Long[] ids);

    /**
     * 删除合同信息信息
     * 
     * @param id 合同信息ID
     * @return 结果
     */
    public int deleteBasicContractById(Long id);
}
