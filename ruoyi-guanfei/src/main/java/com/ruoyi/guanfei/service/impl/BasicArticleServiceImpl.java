package com.ruoyi.guanfei.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.guanfei.domain.BasicArticle;
import com.ruoyi.guanfei.mapper.BasicArticleMapper;
import com.ruoyi.guanfei.service.IBasicArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资讯信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Service
public class BasicArticleServiceImpl implements IBasicArticleService
{
    @Autowired
    private BasicArticleMapper basicArticleMapper;

    /**
     * 查询资讯信息
     * 
     * @param id 资讯信息ID
     * @return 资讯信息
     */
    @Override
    public BasicArticle selectBasicArticleById(Long id)
    {
        return basicArticleMapper.selectBasicArticleById(id);
    }

    /**
     * 查询资讯信息列表
     * 
     * @param basicArticle 资讯信息
     * @return 资讯信息
     */
    @Override
    public List<BasicArticle> selectBasicArticleList(BasicArticle basicArticle)
    {
        return basicArticleMapper.selectBasicArticleList(basicArticle);
    }

    /**
     * 新增资讯信息
     * 
     * @param basicArticle 资讯信息
     * @return 结果
     */
    @Override
    public int insertBasicArticle(BasicArticle basicArticle)
    {
        basicArticle.setCreateTime(DateUtils.getNowDate());
        return basicArticleMapper.insertBasicArticle(basicArticle);
    }

    /**
     * 修改资讯信息
     * 
     * @param basicArticle 资讯信息
     * @return 结果
     */
    @Override
    public int updateBasicArticle(BasicArticle basicArticle)
    {
        return basicArticleMapper.updateBasicArticle(basicArticle);
    }

    /**
     * 批量删除资讯信息
     * 
     * @param ids 需要删除的资讯信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicArticleByIds(Long[] ids)
    {
        return basicArticleMapper.deleteBasicArticleByIds(ids);
    }

    /**
     * 删除资讯信息信息
     * 
     * @param id 资讯信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicArticleById(Long id)
    {
        return basicArticleMapper.deleteBasicArticleById(id);
    }
}
