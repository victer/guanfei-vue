package com.ruoyi.guanfei.service;

import com.ruoyi.guanfei.domain.ShopCouponIssueUser;

import java.util.List;

/**
 * 优惠券领取Service接口
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
public interface IShopCouponIssueUserService 
{
    /**
     * 查询优惠券领取
     * 
     * @param id 优惠券领取ID
     * @return 优惠券领取
     */
    public ShopCouponIssueUser selectShopCouponIssueUserById(Long id);

    /**
     * 查询优惠券领取列表
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 优惠券领取集合
     */
    public List<ShopCouponIssueUser> selectShopCouponIssueUserList(ShopCouponIssueUser shopCouponIssueUser);

    /**
     * 新增优惠券领取
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 结果
     */
    public int insertShopCouponIssueUser(ShopCouponIssueUser shopCouponIssueUser);

    /**
     * 修改优惠券领取
     * 
     * @param shopCouponIssueUser 优惠券领取
     * @return 结果
     */
    public int updateShopCouponIssueUser(ShopCouponIssueUser shopCouponIssueUser);

    /**
     * 批量删除优惠券领取
     * 
     * @param ids 需要删除的优惠券领取ID
     * @return 结果
     */
    public int deleteShopCouponIssueUserByIds(Long[] ids);

    /**
     * 删除优惠券领取信息
     * 
     * @param id 优惠券领取ID
     * @return 结果
     */
    public int deleteShopCouponIssueUserById(Long id);
}
