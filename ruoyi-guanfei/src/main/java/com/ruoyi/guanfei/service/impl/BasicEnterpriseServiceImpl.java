package com.ruoyi.guanfei.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicEnterpriseMapper;
import com.ruoyi.guanfei.domain.BasicEnterprise;
import com.ruoyi.guanfei.service.IBasicEnterpriseService;

/**
 * 农资企业信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Service
public class BasicEnterpriseServiceImpl implements IBasicEnterpriseService 
{
    @Autowired
    private BasicEnterpriseMapper basicEnterpriseMapper;

    /**
     * 查询农资企业信息
     * 
     * @param id 农资企业信息ID
     * @return 农资企业信息
     */
    @Override
    public BasicEnterprise selectBasicEnterpriseById(Long id)
    {
        return basicEnterpriseMapper.selectBasicEnterpriseById(id);
    }

    /**
     * 查询农资企业信息列表
     * 
     * @param basicEnterprise 农资企业信息
     * @return 农资企业信息
     */
    @Override
    public List<BasicEnterprise> selectBasicEnterpriseList(BasicEnterprise basicEnterprise)
    {
        return basicEnterpriseMapper.selectBasicEnterpriseList(basicEnterprise);
    }

    /**
     * 新增农资企业信息
     * 
     * @param basicEnterprise 农资企业信息
     * @return 结果
     */
    @Override
    public int insertBasicEnterprise(BasicEnterprise basicEnterprise)
    {
        return basicEnterpriseMapper.insertBasicEnterprise(basicEnterprise);
    }

    /**
     * 修改农资企业信息
     * 
     * @param basicEnterprise 农资企业信息
     * @return 结果
     */
    @Override
    public int updateBasicEnterprise(BasicEnterprise basicEnterprise)
    {
        return basicEnterpriseMapper.updateBasicEnterprise(basicEnterprise);
    }

    /**
     * 批量删除农资企业信息
     * 
     * @param ids 需要删除的农资企业信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicEnterpriseByIds(Long[] ids)
    {
        return basicEnterpriseMapper.deleteBasicEnterpriseByIds(ids);
    }

    /**
     * 删除农资企业信息信息
     * 
     * @param id 农资企业信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicEnterpriseById(Long id)
    {
        return basicEnterpriseMapper.deleteBasicEnterpriseById(id);
    }
}
