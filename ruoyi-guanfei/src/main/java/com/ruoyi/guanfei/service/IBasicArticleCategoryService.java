package com.ruoyi.guanfei.service;

import com.ruoyi.guanfei.domain.BasicArticleCategory;

import java.util.List;

/**
 * 资讯信息分类Service接口
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
public interface IBasicArticleCategoryService 
{
    /**
     * 查询资讯信息分类
     * 
     * @param id 资讯信息分类ID
     * @return 资讯信息分类
     */
    public BasicArticleCategory selectBasicArticleCategoryById(Long id);

    /**
     * 查询资讯信息分类列表
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 资讯信息分类集合
     */
    public List<BasicArticleCategory> selectBasicArticleCategoryList(BasicArticleCategory basicArticleCategory);

    /**
     * 新增资讯信息分类
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 结果
     */
    public int insertBasicArticleCategory(BasicArticleCategory basicArticleCategory);

    /**
     * 修改资讯信息分类
     * 
     * @param basicArticleCategory 资讯信息分类
     * @return 结果
     */
    public int updateBasicArticleCategory(BasicArticleCategory basicArticleCategory);

    /**
     * 批量删除资讯信息分类
     * 
     * @param ids 需要删除的资讯信息分类ID
     * @return 结果
     */
    public int deleteBasicArticleCategoryByIds(Long[] ids);

    /**
     * 删除资讯信息分类信息
     * 
     * @param id 资讯信息分类ID
     * @return 结果
     */
    public int deleteBasicArticleCategoryById(Long id);
}
