package com.ruoyi.guanfei.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.guanfei.domain.ShopCoupon;
import com.ruoyi.guanfei.mapper.ShopCouponMapper;
import com.ruoyi.guanfei.service.IShopCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 优惠券Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Service
public class ShopCouponServiceImpl implements IShopCouponService
{
    @Autowired
    private ShopCouponMapper shopCouponMapper;

    /**
     * 查询优惠券
     * 
     * @param id 优惠券ID
     * @return 优惠券
     */
    @Override
    public ShopCoupon selectShopCouponById(Long id)
    {
        return shopCouponMapper.selectShopCouponById(id);
    }

    /**
     * 查询优惠券列表
     * 
     * @param shopCoupon 优惠券
     * @return 优惠券
     */
    @Override
    public List<ShopCoupon> selectShopCouponList(ShopCoupon shopCoupon)
    {
        return shopCouponMapper.selectShopCouponList(shopCoupon);
    }

    /**
     * 新增优惠券
     * 
     * @param shopCoupon 优惠券
     * @return 结果
     */
    @Override
    public int insertShopCoupon(ShopCoupon shopCoupon)
    {
        shopCoupon.setCreateTime(DateUtils.getNowDate());
        return shopCouponMapper.insertShopCoupon(shopCoupon);
    }

    /**
     * 修改优惠券
     * 
     * @param shopCoupon 优惠券
     * @return 结果
     */
    @Override
    public int updateShopCoupon(ShopCoupon shopCoupon)
    {
        return shopCouponMapper.updateShopCoupon(shopCoupon);
    }

    /**
     * 批量删除优惠券
     * 
     * @param ids 需要删除的优惠券ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponByIds(Long[] ids)
    {
        return shopCouponMapper.deleteShopCouponByIds(ids);
    }

    /**
     * 删除优惠券信息
     * 
     * @param id 优惠券ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponById(Long id)
    {
        return shopCouponMapper.deleteShopCouponById(id);
    }
}
