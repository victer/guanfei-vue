package com.ruoyi.guanfei.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicPeopleMapper;
import com.ruoyi.guanfei.domain.BasicPeople;
import com.ruoyi.guanfei.service.IBasicPeopleService;

/**
 * 人员信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Service
public class BasicPeopleServiceImpl implements IBasicPeopleService 
{
    @Autowired
    private BasicPeopleMapper basicPeopleMapper;

    @Autowired
    private SysDeptMapper deptMapper;

    /**
     * 查询人员信息
     * 
     * @param id 人员信息ID
     * @return 人员信息
     */
    @Override
    public BasicPeople selectBasicPeopleById(Long id)
    {
        return basicPeopleMapper.selectBasicPeopleById(id);
    }

    /**
     * 查询人员信息列表
     * 
     * @param basicPeople 人员信息
     * @return 人员信息
     */
    @Override
    public List<BasicPeople> selectBasicPeopleList(BasicPeople basicPeople) {
        List<BasicPeople> peopleList = basicPeopleMapper.selectBasicPeopleList(basicPeople);
        for (BasicPeople people : peopleList) {
            SysDept sysDept = deptMapper.selectDeptById(people.getCompanyId());
            people.setCompanyName(sysDept.getDeptName());
        }
        return peopleList;
    }

    /**
     * 新增人员信息
     * 
     * @param basicPeople 人员信息
     * @return 结果
     */
    @Override
    public int insertBasicPeople(BasicPeople basicPeople)
    {
        return basicPeopleMapper.insertBasicPeople(basicPeople);
    }

    /**
     * 修改人员信息
     * 
     * @param basicPeople 人员信息
     * @return 结果
     */
    @Override
    public int updateBasicPeople(BasicPeople basicPeople)
    {
        return basicPeopleMapper.updateBasicPeople(basicPeople);
    }

    /**
     * 批量删除人员信息
     * 
     * @param ids 需要删除的人员信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicPeopleByIds(Long[] ids)
    {
        return basicPeopleMapper.deleteBasicPeopleByIds(ids);
    }

    /**
     * 删除人员信息信息
     * 
     * @param id 人员信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicPeopleById(Long id)
    {
        return basicPeopleMapper.deleteBasicPeopleById(id);
    }

    /**
     * 查询人员类型
     * @param
     * @return 结果
     */
    @Override
    public List<BasicPeople> listNoPaging() {
        return basicPeopleMapper.selectBasicPeopleList(null);
    }
}
