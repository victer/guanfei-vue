package com.ruoyi.guanfei.service.impl;

import com.ruoyi.guanfei.domain.FlyWork;
import com.ruoyi.guanfei.mapper.FlyWorkMapper;
import com.ruoyi.guanfei.service.IFlyWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 无人机作业记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Service
public class FlyWorkServiceImpl implements IFlyWorkService
{
    @Autowired
    private FlyWorkMapper flyWorkMapper;

    /**
     * 查询无人机作业记录
     * 
     * @param id 无人机作业记录ID
     * @return 无人机作业记录
     */
    @Override
    public FlyWork selectFlyWorkById(Long id)
    {
        return flyWorkMapper.selectFlyWorkById(id);
    }

    /**
     * 查询无人机作业记录列表
     * 
     * @param flyWork 无人机作业记录
     * @return 无人机作业记录
     */
    @Override
    public List<FlyWork> selectFlyWorkList(FlyWork flyWork)
    {
        return flyWorkMapper.selectFlyWorkList(flyWork);
    }

    /**
     * 新增无人机作业记录
     * 
     * @param flyWork 无人机作业记录
     * @return 结果
     */
    @Override
    public int insertFlyWork(FlyWork flyWork)
    {
        return flyWorkMapper.insertFlyWork(flyWork);
    }

    /**
     * 修改无人机作业记录
     * 
     * @param flyWork 无人机作业记录
     * @return 结果
     */
    @Override
    public int updateFlyWork(FlyWork flyWork)
    {
        return flyWorkMapper.updateFlyWork(flyWork);
    }

    /**
     * 批量删除无人机作业记录
     * 
     * @param ids 需要删除的无人机作业记录ID
     * @return 结果
     */
    @Override
    public int deleteFlyWorkByIds(Long[] ids)
    {
        return flyWorkMapper.deleteFlyWorkByIds(ids);
    }

    /**
     * 删除无人机作业记录信息
     * 
     * @param id 无人机作业记录ID
     * @return 结果
     */
    @Override
    public int deleteFlyWorkById(Long id)
    {
        return flyWorkMapper.deleteFlyWorkById(id);
    }
}
