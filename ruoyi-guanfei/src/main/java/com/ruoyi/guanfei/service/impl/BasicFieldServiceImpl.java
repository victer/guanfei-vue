package com.ruoyi.guanfei.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicFieldMapper;
import com.ruoyi.guanfei.domain.BasicField;
import com.ruoyi.guanfei.service.IBasicFieldService;

/**
 * 田块信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Service
public class BasicFieldServiceImpl implements IBasicFieldService 
{
    @Autowired
    private BasicFieldMapper basicFieldMapper;

    /**
     * 查询田块信息
     * 
     * @param id 田块信息ID
     * @return 田块信息
     */
    @Override
    public BasicField selectBasicFieldById(Long id)
    {
        return basicFieldMapper.selectBasicFieldById(id);
    }

    /**
     * 查询田块信息列表
     * 
     * @param basicField 田块信息
     * @return 田块信息
     */
    @Override
    public List<BasicField> selectBasicFieldList(BasicField basicField)
    {
        return basicFieldMapper.selectBasicFieldList(basicField);
    }

    /**
     * 新增田块信息
     * 
     * @param basicField 田块信息
     * @return 结果
     */
    @Override
    public int insertBasicField(BasicField basicField)
    {
        return basicFieldMapper.insertBasicField(basicField);
    }

    /**
     * 修改田块信息
     * 
     * @param basicField 田块信息
     * @return 结果
     */
    @Override
    public int updateBasicField(BasicField basicField)
    {
        return basicFieldMapper.updateBasicField(basicField);
    }

    /**
     * 批量删除田块信息
     * 
     * @param ids 需要删除的田块信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicFieldByIds(Long[] ids)
    {
        return basicFieldMapper.deleteBasicFieldByIds(ids);
    }

    /**
     * 删除田块信息信息
     * 
     * @param id 田块信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicFieldById(Long id)
    {
        return basicFieldMapper.deleteBasicFieldById(id);
    }

    /**
     * 查询田块信息（不分页）
     * @return
     * @param
     */
    @Override
    public List<BasicField> listNoPaging() {
        return basicFieldMapper.selectBasicFieldList(null);
    }
}
