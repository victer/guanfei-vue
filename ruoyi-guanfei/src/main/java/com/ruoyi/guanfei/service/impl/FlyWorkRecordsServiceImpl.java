package com.ruoyi.guanfei.service.impl;

import com.ruoyi.guanfei.domain.FlyWorkRecords;
import com.ruoyi.guanfei.mapper.FlyWorkRecordsMapper;
import com.ruoyi.guanfei.service.IFlyWorkRecordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 无人机作业原始记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Service
public class FlyWorkRecordsServiceImpl implements IFlyWorkRecordsService
{
    @Autowired
    private FlyWorkRecordsMapper flyWorkRecordsMapper;

    /**
     * 查询无人机作业原始记录
     * 
     * @param id 无人机作业原始记录ID
     * @return 无人机作业原始记录
     */
    @Override
    public FlyWorkRecords selectFlyWorkRecordsById(Long id)
    {
        return flyWorkRecordsMapper.selectFlyWorkRecordsById(id);
    }

    /**
     * 查询无人机作业原始记录列表
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 无人机作业原始记录
     */
    @Override
    public List<FlyWorkRecords> selectFlyWorkRecordsList(FlyWorkRecords flyWorkRecords)
    {
        return flyWorkRecordsMapper.selectFlyWorkRecordsList(flyWorkRecords);
    }

    /**
     * 新增无人机作业原始记录
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 结果
     */
    @Override
    public int insertFlyWorkRecords(FlyWorkRecords flyWorkRecords)
    {
        return flyWorkRecordsMapper.insertFlyWorkRecords(flyWorkRecords);
    }

    /**
     * 修改无人机作业原始记录
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 结果
     */
    @Override
    public int updateFlyWorkRecords(FlyWorkRecords flyWorkRecords)
    {
        return flyWorkRecordsMapper.updateFlyWorkRecords(flyWorkRecords);
    }

    /**
     * 批量删除无人机作业原始记录
     * 
     * @param ids 需要删除的无人机作业原始记录ID
     * @return 结果
     */
    @Override
    public int deleteFlyWorkRecordsByIds(Long[] ids)
    {
        return flyWorkRecordsMapper.deleteFlyWorkRecordsByIds(ids);
    }

    /**
     * 删除无人机作业原始记录信息
     * 
     * @param id 无人机作业原始记录ID
     * @return 结果
     */
    @Override
    public int deleteFlyWorkRecordsById(Long id)
    {
        return flyWorkRecordsMapper.deleteFlyWorkRecordsById(id);
    }
}
