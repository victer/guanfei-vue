package com.ruoyi.guanfei.service.impl;

import com.ruoyi.guanfei.domain.ShopCouponUser;
import com.ruoyi.guanfei.mapper.ShopCouponUserMapper;
import com.ruoyi.guanfei.service.IShopCouponUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 优惠券发放记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Service
public class ShopCouponUserServiceImpl implements IShopCouponUserService
{
    @Autowired
    private ShopCouponUserMapper shopCouponUserMapper;

    /**
     * 查询优惠券发放记录
     * 
     * @param id 优惠券发放记录ID
     * @return 优惠券发放记录
     */
    @Override
    public ShopCouponUser selectShopCouponUserById(Long id)
    {
        return shopCouponUserMapper.selectShopCouponUserById(id);
    }

    /**
     * 查询优惠券发放记录列表
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 优惠券发放记录
     */
    @Override
    public List<ShopCouponUser> selectShopCouponUserList(ShopCouponUser shopCouponUser)
    {
        return shopCouponUserMapper.selectShopCouponUserList(shopCouponUser);
    }

    /**
     * 新增优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    @Override
    public int insertShopCouponUser(ShopCouponUser shopCouponUser)
    {
        return shopCouponUserMapper.insertShopCouponUser(shopCouponUser);
    }

    /**
     * 修改优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    @Override
    public int updateShopCouponUser(ShopCouponUser shopCouponUser)
    {
        return shopCouponUserMapper.updateShopCouponUser(shopCouponUser);
    }

    /**
     * 批量删除优惠券发放记录
     * 
     * @param ids 需要删除的优惠券发放记录ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponUserByIds(Long[] ids)
    {
        return shopCouponUserMapper.deleteShopCouponUserByIds(ids);
    }

    /**
     * 删除优惠券发放记录信息
     * 
     * @param id 优惠券发放记录ID
     * @return 结果
     */
    @Override
    public int deleteShopCouponUserById(Long id)
    {
        return shopCouponUserMapper.deleteShopCouponUserById(id);
    }
}
