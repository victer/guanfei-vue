package com.ruoyi.guanfei.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.guanfei.mapper.BasicEnterpriseAuditMapper;
import com.ruoyi.guanfei.domain.BasicEnterpriseAudit;
import com.ruoyi.guanfei.service.IBasicEnterpriseAuditService;

/**
 * 农资企业认证信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Service
public class BasicEnterpriseAuditServiceImpl implements IBasicEnterpriseAuditService 
{
    @Autowired
    private BasicEnterpriseAuditMapper basicEnterpriseAuditMapper;

    /**
     * 查询农资企业认证信息
     * 
     * @param id 农资企业认证信息ID
     * @return 农资企业认证信息
     */
    @Override
    public BasicEnterpriseAudit selectBasicEnterpriseAuditById(Long id)
    {
        return basicEnterpriseAuditMapper.selectBasicEnterpriseAuditById(id);
    }

    /**
     * 查询农资企业认证信息列表
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 农资企业认证信息
     */
    @Override
    public List<BasicEnterpriseAudit> selectBasicEnterpriseAuditList(BasicEnterpriseAudit basicEnterpriseAudit)
    {
        return basicEnterpriseAuditMapper.selectBasicEnterpriseAuditList(basicEnterpriseAudit);
    }

    /**
     * 新增农资企业认证信息
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 结果
     */
    @Override
    public int insertBasicEnterpriseAudit(BasicEnterpriseAudit basicEnterpriseAudit)
    {
        basicEnterpriseAudit.setCreateTime(DateUtils.getNowDate());
        return basicEnterpriseAuditMapper.insertBasicEnterpriseAudit(basicEnterpriseAudit);
    }

    /**
     * 修改农资企业认证信息
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 结果
     */
    @Override
    public int updateBasicEnterpriseAudit(BasicEnterpriseAudit basicEnterpriseAudit)
    {
        basicEnterpriseAudit.setUpdateTime(DateUtils.getNowDate());
        return basicEnterpriseAuditMapper.updateBasicEnterpriseAudit(basicEnterpriseAudit);
    }

    /**
     * 批量删除农资企业认证信息
     * 
     * @param ids 需要删除的农资企业认证信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicEnterpriseAuditByIds(Long[] ids)
    {
        return basicEnterpriseAuditMapper.deleteBasicEnterpriseAuditByIds(ids);
    }

    /**
     * 删除农资企业认证信息信息
     * 
     * @param id 农资企业认证信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicEnterpriseAuditById(Long id)
    {
        return basicEnterpriseAuditMapper.deleteBasicEnterpriseAuditById(id);
    }
}
