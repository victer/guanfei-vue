package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicCard;

/**
 * 人员证件信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface BasicCardMapper 
{
    /**
     * 查询人员证件信息
     * 
     * @param id 人员证件信息ID
     * @return 人员证件信息
     */
    public BasicCard selectBasicCardById(Long id);

    /**
     * 查询人员证件信息列表
     * 
     * @param basicCard 人员证件信息
     * @return 人员证件信息集合
     */
    public List<BasicCard> selectBasicCardList(BasicCard basicCard);

    /**
     * 新增人员证件信息
     * 
     * @param basicCard 人员证件信息
     * @return 结果
     */
    public int insertBasicCard(BasicCard basicCard);

    /**
     * 修改人员证件信息
     * 
     * @param basicCard 人员证件信息
     * @return 结果
     */
    public int updateBasicCard(BasicCard basicCard);

    /**
     * 删除人员证件信息
     * 
     * @param id 人员证件信息ID
     * @return 结果
     */
    public int deleteBasicCardById(Long id);

    /**
     * 批量删除人员证件信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicCardByIds(Long[] ids);
}
