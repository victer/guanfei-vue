package com.ruoyi.guanfei.mapper;

import com.ruoyi.guanfei.domain.ShopCoupon;

import java.util.List;

/**
 * 优惠券Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
public interface ShopCouponMapper 
{
    /**
     * 查询优惠券
     * 
     * @param id 优惠券ID
     * @return 优惠券
     */
    public ShopCoupon selectShopCouponById(Long id);

    /**
     * 查询优惠券列表
     * 
     * @param shopCoupon 优惠券
     * @return 优惠券集合
     */
    public List<ShopCoupon> selectShopCouponList(ShopCoupon shopCoupon);

    /**
     * 新增优惠券
     * 
     * @param shopCoupon 优惠券
     * @return 结果
     */
    public int insertShopCoupon(ShopCoupon shopCoupon);

    /**
     * 修改优惠券
     * 
     * @param shopCoupon 优惠券
     * @return 结果
     */
    public int updateShopCoupon(ShopCoupon shopCoupon);

    /**
     * 删除优惠券
     * 
     * @param id 优惠券ID
     * @return 结果
     */
    public int deleteShopCouponById(Long id);

    /**
     * 批量删除优惠券
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopCouponByIds(Long[] ids);
}
