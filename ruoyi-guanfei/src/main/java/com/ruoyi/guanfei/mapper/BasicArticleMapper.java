package com.ruoyi.guanfei.mapper;

import com.ruoyi.guanfei.domain.BasicArticle;

import java.util.List;

/**
 * 资讯信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
public interface BasicArticleMapper 
{
    /**
     * 查询资讯信息
     * 
     * @param id 资讯信息ID
     * @return 资讯信息
     */
    public BasicArticle selectBasicArticleById(Long id);

    /**
     * 查询资讯信息列表
     * 
     * @param basicArticle 资讯信息
     * @return 资讯信息集合
     */
    public List<BasicArticle> selectBasicArticleList(BasicArticle basicArticle);

    /**
     * 新增资讯信息
     * 
     * @param basicArticle 资讯信息
     * @return 结果
     */
    public int insertBasicArticle(BasicArticle basicArticle);

    /**
     * 修改资讯信息
     * 
     * @param basicArticle 资讯信息
     * @return 结果
     */
    public int updateBasicArticle(BasicArticle basicArticle);

    /**
     * 删除资讯信息
     * 
     * @param id 资讯信息ID
     * @return 结果
     */
    public int deleteBasicArticleById(Long id);

    /**
     * 批量删除资讯信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicArticleByIds(Long[] ids);
}
