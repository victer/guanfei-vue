package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicField;

/**
 * 田块信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface BasicFieldMapper 
{
    /**
     * 查询田块信息
     * 
     * @param id 田块信息ID
     * @return 田块信息
     */
    public BasicField selectBasicFieldById(Long id);

    /**
     * 查询田块信息列表
     * 
     * @param basicField 田块信息
     * @return 田块信息集合
     */
    public List<BasicField> selectBasicFieldList(BasicField basicField);

    /**
     * 新增田块信息
     * 
     * @param basicField 田块信息
     * @return 结果
     */
    public int insertBasicField(BasicField basicField);

    /**
     * 修改田块信息
     * 
     * @param basicField 田块信息
     * @return 结果
     */
    public int updateBasicField(BasicField basicField);

    /**
     * 删除田块信息
     * 
     * @param id 田块信息ID
     * @return 结果
     */
    public int deleteBasicFieldById(Long id);

    /**
     * 批量删除田块信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicFieldByIds(Long[] ids);
}
