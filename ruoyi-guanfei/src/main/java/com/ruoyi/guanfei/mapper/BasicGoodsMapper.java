package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicGoods;

/**
 * 农资产品信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
public interface BasicGoodsMapper 
{
    /**
     * 查询农资产品信息
     * 
     * @param id 农资产品信息ID
     * @return 农资产品信息
     */
    public BasicGoods selectBasicGoodsById(Long id);

    /**
     * 查询农资产品信息列表
     * 
     * @param basicGoods 农资产品信息
     * @return 农资产品信息集合
     */
    public List<BasicGoods> selectBasicGoodsList(BasicGoods basicGoods);

    /**
     * 新增农资产品信息
     * 
     * @param basicGoods 农资产品信息
     * @return 结果
     */
    public int insertBasicGoods(BasicGoods basicGoods);

    /**
     * 修改农资产品信息
     * 
     * @param basicGoods 农资产品信息
     * @return 结果
     */
    public int updateBasicGoods(BasicGoods basicGoods);

    /**
     * 删除农资产品信息
     * 
     * @param id 农资产品信息ID
     * @return 结果
     */
    public int deleteBasicGoodsById(Long id);

    /**
     * 批量删除农资产品信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicGoodsByIds(Long[] ids);
}
