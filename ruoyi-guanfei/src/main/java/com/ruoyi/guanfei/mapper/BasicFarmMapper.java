package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicFarm;

/**
 * 农机信息Mapper接口
 *
 * @author ruoyi
 * @date 2021-06-17
 */
public interface BasicFarmMapper
{
    /**
     * 查询农机信息
     *
     * @param id 农机信息ID
     * @return 农机信息
     */
    public BasicFarm selectBasicFarmById(Long id);

    /**
     * 查询农机信息列表
     *
     * @param basicFarm 农机信息
     * @return 农机信息集合
     */
    public List<BasicFarm> selectBasicFarmList(BasicFarm basicFarm);

    /**
     * 根据类型查询最大农机编码
     *
     * @param type 农机类型
     * @return 农机信息
     */
    public BasicFarm selectBasicFarmMaxCodeByType(long type);

    /**
     * 新增农机信息
     *
     * @param basicFarm 农机信息
     * @return 结果
     */
    public int insertBasicFarm(BasicFarm basicFarm);

    /**
     * 修改农机信息
     *
     * @param basicFarm 农机信息
     * @return 结果
     */
    public int updateBasicFarm(BasicFarm basicFarm);

    /**
     * 删除农机信息
     *
     * @param id 农机信息ID
     * @return 结果
     */
    public int deleteBasicFarmById(Long id);

    /**
     * 批量删除农机信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicFarmByIds(Long[] ids);
}
