package com.ruoyi.guanfei.mapper;

import com.ruoyi.guanfei.domain.FlyWork;

import java.util.List;

/**
 * 无人机作业记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
public interface FlyWorkMapper 
{
    /**
     * 查询无人机作业记录
     * 
     * @param id 无人机作业记录ID
     * @return 无人机作业记录
     */
    public FlyWork selectFlyWorkById(Long id);

    /**
     * 查询无人机作业记录列表
     * 
     * @param flyWork 无人机作业记录
     * @return 无人机作业记录集合
     */
    public List<FlyWork> selectFlyWorkList(FlyWork flyWork);

    /**
     * 新增无人机作业记录
     * 
     * @param flyWork 无人机作业记录
     * @return 结果
     */
    public int insertFlyWork(FlyWork flyWork);

    /**
     * 修改无人机作业记录
     * 
     * @param flyWork 无人机作业记录
     * @return 结果
     */
    public int updateFlyWork(FlyWork flyWork);

    /**
     * 删除无人机作业记录
     * 
     * @param id 无人机作业记录ID
     * @return 结果
     */
    public int deleteFlyWorkById(Long id);

    /**
     * 批量删除无人机作业记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFlyWorkByIds(Long[] ids);
}
