package com.ruoyi.guanfei.mapper;

import com.ruoyi.guanfei.domain.FlyWorkRecords;

import java.util.List;

/**
 * 无人机作业原始记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
public interface FlyWorkRecordsMapper 
{
    /**
     * 查询无人机作业原始记录
     * 
     * @param id 无人机作业原始记录ID
     * @return 无人机作业原始记录
     */
    public FlyWorkRecords selectFlyWorkRecordsById(Long id);

    /**
     * 查询无人机作业原始记录列表
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 无人机作业原始记录集合
     */
    public List<FlyWorkRecords> selectFlyWorkRecordsList(FlyWorkRecords flyWorkRecords);

    /**
     * 新增无人机作业原始记录
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 结果
     */
    public int insertFlyWorkRecords(FlyWorkRecords flyWorkRecords);

    /**
     * 修改无人机作业原始记录
     * 
     * @param flyWorkRecords 无人机作业原始记录
     * @return 结果
     */
    public int updateFlyWorkRecords(FlyWorkRecords flyWorkRecords);

    /**
     * 删除无人机作业原始记录
     * 
     * @param id 无人机作业原始记录ID
     * @return 结果
     */
    public int deleteFlyWorkRecordsById(Long id);

    /**
     * 批量删除无人机作业原始记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFlyWorkRecordsByIds(Long[] ids);
}
