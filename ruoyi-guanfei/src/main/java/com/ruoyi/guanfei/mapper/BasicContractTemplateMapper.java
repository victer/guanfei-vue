package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicContractTemplate;

/**
 * 合同模板信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
public interface BasicContractTemplateMapper 
{
    /**
     * 查询合同模板信息
     * 
     * @param id 合同模板信息ID
     * @return 合同模板信息
     */
    public BasicContractTemplate selectBasicContractTemplateById(Long id);

    /**
     * 查询合同模板信息列表
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 合同模板信息集合
     */
    public List<BasicContractTemplate> selectBasicContractTemplateList(BasicContractTemplate basicContractTemplate);

    /**
     * 新增合同模板信息
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 结果
     */
    public int insertBasicContractTemplate(BasicContractTemplate basicContractTemplate);

    /**
     * 修改合同模板信息
     * 
     * @param basicContractTemplate 合同模板信息
     * @return 结果
     */
    public int updateBasicContractTemplate(BasicContractTemplate basicContractTemplate);

    /**
     * 删除合同模板信息
     * 
     * @param id 合同模板信息ID
     * @return 结果
     */
    public int deleteBasicContractTemplateById(Long id);

    /**
     * 批量删除合同模板信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicContractTemplateByIds(Long[] ids);
}
