package com.ruoyi.guanfei.mapper;

import java.util.List;
import com.ruoyi.guanfei.domain.BasicEnterpriseAudit;

/**
 * 农资企业认证信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
public interface BasicEnterpriseAuditMapper 
{
    /**
     * 查询农资企业认证信息
     * 
     * @param id 农资企业认证信息ID
     * @return 农资企业认证信息
     */
    public BasicEnterpriseAudit selectBasicEnterpriseAuditById(Long id);

    /**
     * 查询农资企业认证信息列表
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 农资企业认证信息集合
     */
    public List<BasicEnterpriseAudit> selectBasicEnterpriseAuditList(BasicEnterpriseAudit basicEnterpriseAudit);

    /**
     * 新增农资企业认证信息
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 结果
     */
    public int insertBasicEnterpriseAudit(BasicEnterpriseAudit basicEnterpriseAudit);

    /**
     * 修改农资企业认证信息
     * 
     * @param basicEnterpriseAudit 农资企业认证信息
     * @return 结果
     */
    public int updateBasicEnterpriseAudit(BasicEnterpriseAudit basicEnterpriseAudit);

    /**
     * 删除农资企业认证信息
     * 
     * @param id 农资企业认证信息ID
     * @return 结果
     */
    public int deleteBasicEnterpriseAuditById(Long id);

    /**
     * 批量删除农资企业认证信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicEnterpriseAuditByIds(Long[] ids);
}
