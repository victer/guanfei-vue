package com.ruoyi.guanfei.mapper;

import com.ruoyi.guanfei.domain.ShopCouponUser;

import java.util.List;

/**
 * 优惠券发放记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
public interface ShopCouponUserMapper 
{
    /**
     * 查询优惠券发放记录
     * 
     * @param id 优惠券发放记录ID
     * @return 优惠券发放记录
     */
    public ShopCouponUser selectShopCouponUserById(Long id);

    /**
     * 查询优惠券发放记录列表
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 优惠券发放记录集合
     */
    public List<ShopCouponUser> selectShopCouponUserList(ShopCouponUser shopCouponUser);

    /**
     * 新增优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    public int insertShopCouponUser(ShopCouponUser shopCouponUser);

    /**
     * 修改优惠券发放记录
     * 
     * @param shopCouponUser 优惠券发放记录
     * @return 结果
     */
    public int updateShopCouponUser(ShopCouponUser shopCouponUser);

    /**
     * 删除优惠券发放记录
     * 
     * @param id 优惠券发放记录ID
     * @return 结果
     */
    public int deleteShopCouponUserById(Long id);

    /**
     * 批量删除优惠券发放记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopCouponUserByIds(Long[] ids);
}
