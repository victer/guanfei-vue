package com.ruoyi.guanfei.controller;

import java.util.List;

import com.ruoyi.guanfei.domain.BasicPeople;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicField;
import com.ruoyi.guanfei.service.IBasicFieldService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 田块信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "田块信息管理")
@RestController
@RequestMapping("/guanfei/basic/field")
public class BasicFieldController extends BaseController
{
    @Autowired
    private IBasicFieldService basicFieldService;

    /**
     * 查询田块信息列表
     */
    @ApiOperation("查询田块信息列表")
    @PreAuthorize("@ss.hasPermi('basic:field:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicField basicField)
    {
        startPage();
        List<BasicField> list = basicFieldService.selectBasicFieldList(basicField);
        return getDataTable(list);
    }

    /**
     * 导出田块信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:field:export')")
    @Log(title = "田块信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicField basicField)
    {
        List<BasicField> list = basicFieldService.selectBasicFieldList(basicField);
        ExcelUtil<BasicField> util = new ExcelUtil<BasicField>(BasicField.class);
        return util.exportExcel(list, "田块信息数据");
    }

    /**
     * 获取田块信息详细信息
     */
    @ApiOperation("获取田块信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:field:query')")
    @GetMapping(value = "getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicFieldService.selectBasicFieldById(id));
    }

    /**
     * 新增田块信息
     */
    @ApiOperation("新增田块信息")
    @ApiImplicitParam(name = "basicField", value = "新增田块信息", dataType = "BasicField")
    @PreAuthorize("@ss.hasPermi('basic:field:add')")
    @Log(title = "田块信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicField basicField)
    {
        return toAjax(basicFieldService.insertBasicField(basicField));
    }

    /**
     * 修改田块信息
     */
    @ApiOperation("修改田块信息")
    @ApiImplicitParam(name = "basicField", value = "修改田块信息", dataType = "BasicField")
    @PreAuthorize("@ss.hasPermi('basic:field:edit')")
    @Log(title = "田块信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicField basicField)
    {
        return toAjax(basicFieldService.updateBasicField(basicField));
    }

    /**
     * 删除田块信息
     */
    @ApiOperation("删除田块信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:field:remove')")
    @Log(title = "田块信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicFieldService.deleteBasicFieldByIds(ids));
    }

    /**
     * 查询田块信息列表(不分页)
     */
    @ApiOperation("查询田块信息列表(不分页,select选择使用)")
    @PreAuthorize("@ss.hasPermi('basic:field:list')")
    @GetMapping("/listNoPaging")
    public AjaxResult listNoPaging()
    {
        List<BasicField> fields = basicFieldService.listNoPaging();
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("fields",fields);
        return ajaxResult;
    }
}
