package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicPeople;
import com.ruoyi.guanfei.service.IBasicPeopleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 人员信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "人员信息管理")
@RestController
    @RequestMapping("/guanfei/basic/people")
public class BasicPeopleController extends BaseController
{
    @Autowired
    private IBasicPeopleService basicPeopleService;

    /**
     * 查询人员信息列表
     */
    @ApiOperation("查询人员信息列表")
    @PreAuthorize("@ss.hasPermi('basic:people:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicPeople basicPeople)
    {
        startPage();
        List<BasicPeople> list = basicPeopleService.selectBasicPeopleList(basicPeople);
        return getDataTable(list);
    }

    /**
     * 导出人员信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:people:export')")
    @Log(title = "人员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicPeople basicPeople)
    {
        List<BasicPeople> list = basicPeopleService.selectBasicPeopleList(basicPeople);
        ExcelUtil<BasicPeople> util = new ExcelUtil<BasicPeople>(BasicPeople.class);
        return util.exportExcel(list, "人员信息数据");
    }

    /**
     * 获取人员信息详细信息
     */
    @ApiOperation("获取人员信息详细")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:people:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicPeopleService.selectBasicPeopleById(id));
    }

    /**
     * 新增人员信息
     */
    @ApiOperation("新增人员")
    @ApiImplicitParam(name = "basicPeople", value = "新增人员信息", dataType = "BasicPeople")
    @PreAuthorize("@ss.hasPermi('basic:people:add')")
    @Log(title = "人员信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicPeople basicPeople)
    {
        return toAjax(basicPeopleService.insertBasicPeople(basicPeople));
    }

    /**
     * 修改人员信息
     */
    @ApiOperation("修改人员")
    @ApiImplicitParam(name = "basicPeople", value = "修改人员信息", dataType = "BasicPeople")
    @PreAuthorize("@ss.hasPermi('basic:people:edit')")
    @Log(title = "人员信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicPeople basicPeople)
    {
        return toAjax(basicPeopleService.updateBasicPeople(basicPeople));
    }

    /**
     * 删除人员信息
     */
    @ApiOperation("删除人员信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:people:remove')")
    @Log(title = "人员信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicPeopleService.deleteBasicPeopleByIds(ids));
    }

    /**
     * 查询人员信息列表(不分页)
     */
    @ApiOperation("查询人员信息列表(不分页,select选择使用)")
    @PreAuthorize("@ss.hasPermi('basic:people:list')")
    @GetMapping("/listNoPaging")
    public AjaxResult listNoPaging()
    {
        List<BasicPeople> peoples = basicPeopleService.listNoPaging();
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("peoples",peoples);
        return ajaxResult;
    }
}
