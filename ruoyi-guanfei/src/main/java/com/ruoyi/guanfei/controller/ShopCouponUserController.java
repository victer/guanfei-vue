package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.ShopCouponUser;
import com.ruoyi.guanfei.service.IShopCouponUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 优惠券发放记录Controller
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Api(tags = "优惠券发放记录")
@RestController
@RequestMapping("/guanfei/basic/user")
public class ShopCouponUserController extends BaseController
{
    @Autowired
    private IShopCouponUserService shopCouponUserService;

    /**
     * 查询优惠券发放记录列表
     */
    @ApiOperation("查询优惠券发放记录列表")
    @PreAuthorize("@ss.hasPermi('basic:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShopCouponUser shopCouponUser)
    {
        startPage();
        List<ShopCouponUser> list = shopCouponUserService.selectShopCouponUserList(shopCouponUser);
        return getDataTable(list);
    }

    /**
     * 导出优惠券发放记录列表
     */
    @ApiOperation("导出优惠券发放记录列表")
    @PreAuthorize("@ss.hasPermi('basic:user:export')")
    @Log(title = "优惠券发放记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ShopCouponUser shopCouponUser)
    {
        List<ShopCouponUser> list = shopCouponUserService.selectShopCouponUserList(shopCouponUser);
        ExcelUtil<ShopCouponUser> util = new ExcelUtil<ShopCouponUser>(ShopCouponUser.class);
        return util.exportExcel(list, "优惠券发放记录数据");
    }

    /**
     * 获取优惠券发放记录详细信息
     */
    @ApiOperation("获取优惠券发放记录详细信息")
    @PreAuthorize("@ss.hasPermi('basic:user:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(shopCouponUserService.selectShopCouponUserById(id));
    }

    /**
     * 新增优惠券发放记录
     */
    @ApiOperation("新增优惠券发放记录")
    @PreAuthorize("@ss.hasPermi('basic:user:add')")
    @Log(title = "优惠券发放记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ShopCouponUser shopCouponUser)
    {
        return toAjax(shopCouponUserService.insertShopCouponUser(shopCouponUser));
    }

    /**
     * 修改优惠券发放记录
     */
    @ApiOperation("修改优惠券发放记录")
    @PreAuthorize("@ss.hasPermi('basic:user:edit')")
    @Log(title = "优惠券发放记录", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody ShopCouponUser shopCouponUser)
    {
        return toAjax(shopCouponUserService.updateShopCouponUser(shopCouponUser));
    }

    /**
     * 删除优惠券发放记录
     */
    @ApiOperation("删除优惠券发放记录")
    @PreAuthorize("@ss.hasPermi('basic:user:remove')")
    @Log(title = "优惠券发放记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shopCouponUserService.deleteShopCouponUserByIds(ids));
    }
}
