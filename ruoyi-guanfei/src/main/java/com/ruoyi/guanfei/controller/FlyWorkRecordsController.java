package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.FlyWorkRecords;
import com.ruoyi.guanfei.service.IFlyWorkRecordsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 无人机作业原始记录Controller
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Api(tags = "无人机作业原始记录")
@RestController
@RequestMapping("/guanfei/basic/records")
public class FlyWorkRecordsController extends BaseController
{
    @Autowired
    private IFlyWorkRecordsService flyWorkRecordsService;

    /**
     * 查询无人机作业原始记录列表
     */
    @ApiOperation("查询无人机作业原始记录列表")
    @PreAuthorize("@ss.hasPermi('basic:records:list')")
    @GetMapping("/list")
    public TableDataInfo list(FlyWorkRecords flyWorkRecords)
    {
        startPage();
        List<FlyWorkRecords> list = flyWorkRecordsService.selectFlyWorkRecordsList(flyWorkRecords);
        return getDataTable(list);
    }

    /**
     * 导出无人机作业原始记录列表
     */
    @ApiOperation("导出无人机作业原始记录列表")
    @PreAuthorize("@ss.hasPermi('basic:records:export')")
    @Log(title = "无人机作业原始记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FlyWorkRecords flyWorkRecords)
    {
        List<FlyWorkRecords> list = flyWorkRecordsService.selectFlyWorkRecordsList(flyWorkRecords);
        ExcelUtil<FlyWorkRecords> util = new ExcelUtil<FlyWorkRecords>(FlyWorkRecords.class);
        return util.exportExcel(list, "无人机作业原始记录数据");
    }

    /**
     * 获取无人机作业原始记录详细信息
     */
    @ApiOperation("获取无人机作业原始记录详细信息")
    @PreAuthorize("@ss.hasPermi('basic:records:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(flyWorkRecordsService.selectFlyWorkRecordsById(id));
    }

    /**
     * 新增无人机作业原始记录
     */
    @ApiOperation("新增无人机作业原始记录")
    @PreAuthorize("@ss.hasPermi('basic:records:add')")
    @Log(title = "无人机作业原始记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody FlyWorkRecords flyWorkRecords)
    {
        return toAjax(flyWorkRecordsService.insertFlyWorkRecords(flyWorkRecords));
    }

    /**
     * 修改无人机作业原始记录
     */
    @ApiOperation("修改无人机作业原始记录")
    @PreAuthorize("@ss.hasPermi('basic:records:edit')")
    @Log(title = "无人机作业原始记录", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody FlyWorkRecords flyWorkRecords)
    {
        return toAjax(flyWorkRecordsService.updateFlyWorkRecords(flyWorkRecords));
    }

    /**
     * 删除无人机作业原始记录
     */
    @ApiOperation("删除无人机作业原始记录")
    @PreAuthorize("@ss.hasPermi('basic:records:remove')")
    @Log(title = "无人机作业原始记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(flyWorkRecordsService.deleteFlyWorkRecordsByIds(ids));
    }
}
