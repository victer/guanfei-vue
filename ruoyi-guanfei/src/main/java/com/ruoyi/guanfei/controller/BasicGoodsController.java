package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicGoods;
import com.ruoyi.guanfei.service.IBasicGoodsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 农资产品信息Controller
 *
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "农资产品信息管理")
@RestController
@RequestMapping("/guanfei/basic/goods")
public class BasicGoodsController extends BaseController {
    @Autowired
    private IBasicGoodsService basicGoodsService;

    /**
     * 查询农资产品信息列表
     */
    @ApiOperation("查询农资产品信息列表")
    @PreAuthorize("@ss.hasPermi('basic:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicGoods basicGoods) {
        startPage();
        List<BasicGoods> list = basicGoodsService.selectBasicGoodsList(basicGoods);
        return getDataTable(list);
    }

    /**
     * 查询农资产品信息列表不分页
     */
    @ApiOperation("查询农资产品信息列表不分页")
    @PreAuthorize("@ss.hasPermi('basic:goods:list')")
    @GetMapping("/listNoPage")
    public AjaxResult listNoPage(BasicGoods basicGoods) {
        List<BasicGoods> list = basicGoodsService.selectBasicGoodsList(basicGoods);
        return AjaxResult.success(list);
    }

    /**
     * 导出农资产品信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:goods:export')")
    @Log(title = "农资产品信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicGoods basicGoods) {
        List<BasicGoods> list = basicGoodsService.selectBasicGoodsList(basicGoods);
        ExcelUtil<BasicGoods> util = new ExcelUtil<BasicGoods>(BasicGoods.class);
        return util.exportExcel(list, "农资产品信息数据");
    }

    /**
     * 获取农资产品信息详细信息
     */
    @ApiOperation("获取农资产品信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:goods:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(basicGoodsService.selectBasicGoodsById(id));
    }

    /**
     * 新增农资产品信息
     */
    @ApiOperation("新增农资产品")
    @ApiImplicitParam(name = "basicGoods", value = "新增农资产品信息", dataType = "BasicGoods")
    @PreAuthorize("@ss.hasPermi('basic:goods:add')")
    @Log(title = "农资产品信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicGoods basicGoods) {
        return toAjax(basicGoodsService.insertBasicGoods(basicGoods));
    }

    /**
     * 修改农资产品信息
     */
    @ApiOperation("修改农资产品")
    @ApiImplicitParam(name = "basicGoods", value = "修改农资产品信息", dataType = "BasicGoods")
    @PreAuthorize("@ss.hasPermi('basic:goods:edit')")
    @Log(title = "农资产品信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicGoods basicGoods) {
        return toAjax(basicGoodsService.updateBasicGoods(basicGoods));
    }

    /**
     * 删除农资产品信息
     */
    @ApiOperation("删除农资产品信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:goods:remove')")
    @Log(title = "农资产品信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(basicGoodsService.deleteBasicGoodsByIds(ids));
    }
}
