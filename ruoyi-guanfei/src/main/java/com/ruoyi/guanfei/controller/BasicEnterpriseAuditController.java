package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicEnterpriseAudit;
import com.ruoyi.guanfei.service.IBasicEnterpriseAuditService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 农资企业认证信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Api(tags = "农资企业认证信息")
@RestController
@RequestMapping("/guanfei/basic/audit")
public class BasicEnterpriseAuditController extends BaseController
{
    @Autowired
    private IBasicEnterpriseAuditService basicEnterpriseAuditService;

    /**
     * 查询农资企业认证信息列表
     */
    @ApiOperation("查询农资企业认证信息列表")
    @PreAuthorize("@ss.hasPermi('guanfei:audit:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicEnterpriseAudit basicEnterpriseAudit)
    {
        startPage();
        List<BasicEnterpriseAudit> list = basicEnterpriseAuditService.selectBasicEnterpriseAuditList(basicEnterpriseAudit);
        return getDataTable(list);
    }

    /**
     * 导出农资企业认证信息列表
     */
    @PreAuthorize("@ss.hasPermi('guanfei:audit:export')")
    @Log(title = "农资企业认证信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicEnterpriseAudit basicEnterpriseAudit)
    {
        List<BasicEnterpriseAudit> list = basicEnterpriseAuditService.selectBasicEnterpriseAuditList(basicEnterpriseAudit);
        ExcelUtil<BasicEnterpriseAudit> util = new ExcelUtil<BasicEnterpriseAudit>(BasicEnterpriseAudit.class);
        return util.exportExcel(list, "农资企业认证信息数据");
    }

    /**
     * 获取农资企业认证信息详细信息
     */
    @ApiOperation("获取农资企业认证信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:audit:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicEnterpriseAuditService.selectBasicEnterpriseAuditById(id));
    }

    /**
     * 新增农资企业认证信息
     */
    @ApiOperation("新增农资企业认证信息")
    @ApiImplicitParam(name = "basicEnterpriseAudit", value = "新增农资企业认证信息", dataType = "BasicEnterpriseAudit")
    @PreAuthorize("@ss.hasPermi('guanfei:audit:add')")
    @Log(title = "农资企业认证信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicEnterpriseAudit basicEnterpriseAudit)
    {
        return toAjax(basicEnterpriseAuditService.insertBasicEnterpriseAudit(basicEnterpriseAudit));
    }

    /**
     * 修改农资企业认证信息
     */
    @ApiOperation("修改农资企业认证信息")
    @ApiImplicitParam(name = "basicEnterpriseAudit", value = "修改农资企业认证信息", dataType = "BasicEnterpriseAudit")
    @PreAuthorize("@ss.hasPermi('guanfei:audit:edit')")
    @Log(title = "农资企业认证信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicEnterpriseAudit basicEnterpriseAudit)
    {
        return toAjax(basicEnterpriseAuditService.updateBasicEnterpriseAudit(basicEnterpriseAudit));
    }

    /**
     * 删除农资企业认证信息
     */
    @ApiOperation("删除农资企业认证信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:audit:remove')")
    @Log(title = "农资企业认证信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicEnterpriseAuditService.deleteBasicEnterpriseAuditByIds(ids));
    }
}
