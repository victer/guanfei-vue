package com.ruoyi.guanfei.controller;

import java.util.List;

import com.ruoyi.guanfei.domain.BasicArticle;
import com.ruoyi.guanfei.service.IBasicArticleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资讯信息Controller
 *
 * @author ruoyi
 * @date 2021-06-29
 */
@RestController
@RequestMapping("/guanfei/basic/article")
public class BasicArticleController extends BaseController {
    @Autowired
    private IBasicArticleService basicArticleService;

    /**
     * 查询资讯信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:article:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicArticle basicArticle) {
        startPage();
        List<BasicArticle> list = basicArticleService.selectBasicArticleList(basicArticle);
        return getDataTable(list);
    }

    /**
     * 导出资讯信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:article:export')")
    @Log(title = "资讯信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicArticle basicArticle) {
        List<BasicArticle> list = basicArticleService.selectBasicArticleList(basicArticle);
        ExcelUtil<BasicArticle> util = new ExcelUtil<BasicArticle>(BasicArticle.class);
        return util.exportExcel(list, "资讯信息数据");
    }

    /**
     * 获取资讯信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('basic:article:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(basicArticleService.selectBasicArticleById(id));
    }

    /**
     * 新增资讯信息
     */
    @PreAuthorize("@ss.hasPermi('basic:article:add')")
    @Log(title = "资讯信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicArticle basicArticle) {
        return toAjax(basicArticleService.insertBasicArticle(basicArticle));
    }

    /**
     * 修改资讯信息
     */
    @PreAuthorize("@ss.hasPermi('basic:article:edit')")
    @Log(title = "资讯信息", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody BasicArticle basicArticle) {
        return toAjax(basicArticleService.updateBasicArticle(basicArticle));
    }

    /**
     * 删除资讯信息
     */
    @PreAuthorize("@ss.hasPermi('basic:article:remove')")
    @Log(title = "资讯信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(basicArticleService.deleteBasicArticleByIds(ids));
    }
}
