package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicEnterprise;
import com.ruoyi.guanfei.service.IBasicEnterpriseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 农资企业信息Controller
 *
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "农资企业信息管理")
@RestController
@RequestMapping("/guanfei/basic/enterprise")
public class BasicEnterpriseController extends BaseController {
    @Autowired
    private IBasicEnterpriseService basicEnterpriseService;

    /**
     * 查询农资企业信息列表
     */
    @ApiOperation("查询农资企业信息列表")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicEnterprise basicEnterprise) {
        startPage();
        List<BasicEnterprise> list = basicEnterpriseService.selectBasicEnterpriseList(basicEnterprise);
        return getDataTable(list);
    }

    /**
     * 查询农资企业信息列表不分页
     */
    @ApiOperation("查询农资企业信息列表不分页")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:list')")
    @GetMapping("/listNoPage")
    public AjaxResult listNoPage(BasicEnterprise basicEnterprise) {
        List<BasicEnterprise> list = basicEnterpriseService.selectBasicEnterpriseList(basicEnterprise);
        return AjaxResult.success(list);
    }

    /**
     * 导出农资企业信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:enterprise:export')")
    @Log(title = "农资企业信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicEnterprise basicEnterprise) {
        List<BasicEnterprise> list = basicEnterpriseService.selectBasicEnterpriseList(basicEnterprise);
        ExcelUtil<BasicEnterprise> util = new ExcelUtil<BasicEnterprise>(BasicEnterprise.class);
        return util.exportExcel(list, "农资企业信息数据");
    }

    /**
     * 获取农资企业信息详细信息
     */
    @ApiOperation("获取农资企业信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(basicEnterpriseService.selectBasicEnterpriseById(id));
    }

    /**
     * 新增农资企业信息
     */
    @ApiOperation("新增用户")
    @ApiImplicitParam(name = "userEntity", value = "新增用户信息", dataType = "UserEntity")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:add')")
    @Log(title = "农资企业信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicEnterprise basicEnterprise) {
        return toAjax(basicEnterpriseService.insertBasicEnterprise(basicEnterprise));
    }

    /**
     * 修改农资企业信息
     */
    @ApiOperation("修改农资企业信息")
    @ApiImplicitParam(name = "basicEnterprise", value = "修改农资企业信息", dataType = "BasicEnterprise")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:edit')")
    @Log(title = "农资企业信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicEnterprise basicEnterprise) {
        return toAjax(basicEnterpriseService.updateBasicEnterprise(basicEnterprise));
    }

    /**
     * 删除农资企业信息
     */
    @ApiOperation("删除农资企业信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:enterprise:remove')")
    @Log(title = "农资企业信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(basicEnterpriseService.deleteBasicEnterpriseByIds(ids));
    }
}
