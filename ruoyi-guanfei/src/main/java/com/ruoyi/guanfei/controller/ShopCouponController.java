package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.ShopCoupon;
import com.ruoyi.guanfei.service.IShopCouponService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 优惠券Controller
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Api(tags = "优惠券")
@RestController
@RequestMapping("/guanfei/basic/coupon")
public class ShopCouponController extends BaseController
{
    @Autowired
    private IShopCouponService shopCouponService;

    /**
     * 查询优惠券列表
     */
    @ApiOperation("查询优惠券列表")
    @PreAuthorize("@ss.hasPermi('basic:coupon:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShopCoupon shopCoupon)
    {
        startPage();
        List<ShopCoupon> list = shopCouponService.selectShopCouponList(shopCoupon);
        return getDataTable(list);
    }

    /**
     * 导出优惠券列表
     */
    @ApiOperation("导出优惠券列表")
    @PreAuthorize("@ss.hasPermi('basic:coupon:export')")
    @Log(title = "优惠券", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ShopCoupon shopCoupon)
    {
        List<ShopCoupon> list = shopCouponService.selectShopCouponList(shopCoupon);
        ExcelUtil<ShopCoupon> util = new ExcelUtil<ShopCoupon>(ShopCoupon.class);
        return util.exportExcel(list, "优惠券数据");
    }

    /**
     * 获取优惠券详细信息
     */
    @ApiOperation("获取优惠券详细信息")
    @PreAuthorize("@ss.hasPermi('basic:coupon:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(shopCouponService.selectShopCouponById(id));
    }

    /**
     * 新增优惠券
     */
    @ApiOperation("新增优惠券")
    @PreAuthorize("@ss.hasPermi('basic:coupon:add')")
    @Log(title = "优惠券", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ShopCoupon shopCoupon)
    {
        return toAjax(shopCouponService.insertShopCoupon(shopCoupon));
    }

    /**
     * 修改优惠券
     */
    @ApiOperation("修改优惠券")
    @PreAuthorize("@ss.hasPermi('basic:coupon:edit')")
    @Log(title = "优惠券", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody ShopCoupon shopCoupon)
    {
        return toAjax(shopCouponService.updateShopCoupon(shopCoupon));
    }

    /**
     * 删除优惠券
     */
    @ApiOperation("删除优惠券")
    @PreAuthorize("@ss.hasPermi('basic:coupon:remove')")
    @Log(title = "优惠券", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shopCouponService.deleteShopCouponByIds(ids));
    }
}
