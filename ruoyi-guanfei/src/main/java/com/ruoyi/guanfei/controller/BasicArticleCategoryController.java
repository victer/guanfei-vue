package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.BasicArticleCategory;
import com.ruoyi.guanfei.service.IBasicArticleCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 资讯信息分类Controller
 *
 * @author ruoyi
 * @date 2021-06-29
 */
@Api(tags = "资讯信息分类")
@RestController
@RequestMapping("/guanfei/basic/category")
public class BasicArticleCategoryController extends BaseController {
    @Autowired
    private IBasicArticleCategoryService basicArticleCategoryService;

    /**
     * 查询资讯信息分类列表
     */
    @ApiOperation("查询资讯信息分类列表")
    @PreAuthorize("@ss.hasPermi('basic:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicArticleCategory basicArticleCategory) {
        startPage();
        List<BasicArticleCategory> list = basicArticleCategoryService.selectBasicArticleCategoryList(basicArticleCategory);
        return getDataTable(list);
    }

    /**
     * 查询资讯信息分类列表不分页
     */
    @ApiOperation("查询农资产品信息列表不分页")
    @PreAuthorize("@ss.hasPermi('basic:category:list')")
    @GetMapping("/listNoPage")
    public AjaxResult listNoPage(BasicArticleCategory basicArticleCategory) {
        List<BasicArticleCategory> list = basicArticleCategoryService.selectBasicArticleCategoryList(basicArticleCategory);
        return AjaxResult.success(list);
    }

    /**
     * 导出资讯信息分类列表
     */
    @ApiOperation("导出资讯信息分类列表")
    @PreAuthorize("@ss.hasPermi('basic:category:export')")
    @Log(title = "资讯信息分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicArticleCategory basicArticleCategory) {
        List<BasicArticleCategory> list = basicArticleCategoryService.selectBasicArticleCategoryList(basicArticleCategory);
        ExcelUtil<BasicArticleCategory> util = new ExcelUtil<BasicArticleCategory>(BasicArticleCategory.class);
        return util.exportExcel(list, "资讯信息分类数据");
    }

    /**
     * 获取资讯信息分类详细信息
     */
    @ApiOperation("获取资讯信息分类详细信息")
    @PreAuthorize("@ss.hasPermi('basic:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(basicArticleCategoryService.selectBasicArticleCategoryById(id));
    }

    /**
     * 新增资讯信息分类
     */
    @ApiOperation("新增资讯信息分类")
    @PreAuthorize("@ss.hasPermi('basic:category:add')")
    @Log(title = "资讯信息分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicArticleCategory basicArticleCategory) {
        return toAjax(basicArticleCategoryService.insertBasicArticleCategory(basicArticleCategory));
    }

    /**
     * 修改资讯信息分类
     */
    @ApiOperation("修改资讯信息分类")
    @PreAuthorize("@ss.hasPermi('basic:category:edit')")
    @Log(title = "资讯信息分类", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody BasicArticleCategory basicArticleCategory) {
        return toAjax(basicArticleCategoryService.updateBasicArticleCategory(basicArticleCategory));
    }

    /**
     * 删除资讯信息分类
     */
    @ApiOperation("删除资讯信息分类")
    @PreAuthorize("@ss.hasPermi('basic:category:remove')")
    @Log(title = "资讯信息分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(basicArticleCategoryService.deleteBasicArticleCategoryByIds(ids));
    }
}
