package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicFarm;
import com.ruoyi.guanfei.service.IBasicFarmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 农机信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "农机信息管理")
@RestController
@RequestMapping("/guanfei/basic/farm")
public class BasicFarmController extends BaseController
{
    @Autowired
    private IBasicFarmService basicFarmService;

    /**
     * 查询农机信息列表
     */
    @ApiOperation("查询农机信息列表")
    @PreAuthorize("@ss.hasPermi('basic:farm:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicFarm basicFarm)
    {
        startPage();
        List<BasicFarm> list = basicFarmService.selectBasicFarmList(basicFarm);
        return getDataTable(list);
    }

    /**
     * 导出农机信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:farm:export')")
    @Log(title = "农机信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicFarm basicFarm)
    {
        List<BasicFarm> list = basicFarmService.selectBasicFarmList(basicFarm);
        ExcelUtil<BasicFarm> util = new ExcelUtil<BasicFarm>(BasicFarm.class);
        return util.exportExcel(list, "农机信息数据");
    }

    /**
     * 获取农机信息详细信息
     */
    @ApiOperation("获取农机信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:farm:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicFarmService.selectBasicFarmById(id));
    }

    /**
     * 新增农机信息
     */
    @ApiOperation("新增农机信息")
    @ApiImplicitParam(name = "basicFarm", value = "新增农机信息", dataType = "BasicFarm")
    @PreAuthorize("@ss.hasPermi('basic:farm:add')")
    @Log(title = "农机信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicFarm basicFarm)
    {
        return toAjax(basicFarmService.insertBasicFarm(basicFarm));
    }

    /**
     * 修改农机信息
     */
    @ApiOperation("修改农机信息")
    @ApiImplicitParam(name = "basicFarm", value = "修改农机信息", dataType = "BasicFarm")
    @PreAuthorize("@ss.hasPermi('basic:farm:edit')")
    @Log(title = "农机信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicFarm basicFarm)
    {
        return toAjax(basicFarmService.updateBasicFarm(basicFarm));
    }

    /**
     * 删除农机信息
     */
    @ApiOperation("删除农机信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:farm:remove')")
    @Log(title = "农机信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicFarmService.deleteBasicFarmByIds(ids));
    }
}
