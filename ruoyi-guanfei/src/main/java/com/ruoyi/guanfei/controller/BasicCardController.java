package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicCard;
import com.ruoyi.guanfei.service.IBasicCardService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 人员证件信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Api(tags = "人员证件信息管理")
@RestController
@RequestMapping("/guanfei/basic/card")
public class BasicCardController extends BaseController
{
    @Autowired
    private IBasicCardService basicCardService;

    /**
     * 查询人员证件信息列表
     */
    @ApiOperation("查询人员证件信息列表")
    @PreAuthorize("@ss.hasPermi('basic:card:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicCard basicCard)
    {
        startPage();
        List<BasicCard> list = basicCardService.selectBasicCardList(basicCard);
        return getDataTable(list);
    }

    /**
     * 导出人员证件信息列表
     */
    @PreAuthorize("@ss.hasPermi('basic:card:export')")
    @Log(title = "人员证件信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicCard basicCard)
    {
        List<BasicCard> list = basicCardService.selectBasicCardList(basicCard);
        ExcelUtil<BasicCard> util = new ExcelUtil<BasicCard>(BasicCard.class);
        return util.exportExcel(list, "人员证件信息数据");
    }

    /**
     * 获取人员证件信息详细信息
     */
    @ApiOperation("获取人员证件信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:card:query')")
    @GetMapping("/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicCardService.selectBasicCardById(id));
    }

    /**
     * 新增人员证件信息
     */
    @ApiOperation("新增人员证件信息")
    @ApiImplicitParam(name = "basicCard", value = "新增人员证件信息", dataType = "BasicCard")
    @PreAuthorize("@ss.hasPermi('basic:card:add')")
    @Log(title = "人员证件信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicCard basicCard)
    {
        return toAjax(basicCardService.insertBasicCard(basicCard));
    }

    /**
     * 修改人员证件信息
     */
    @ApiOperation("修改人员证件信息")
    @ApiImplicitParam(name = "basicCard", value = "修改人员证件信息", dataType = "BasicCard")
    @PreAuthorize("@ss.hasPermi('basic:card:edit')")
    @Log(title = "人员证件信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicCard basicCard)
    {
        return toAjax(basicCardService.updateBasicCard(basicCard));
    }

    /**
     * 删除人员证件信息
     */
    @ApiOperation("删除人员证件信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('basic:card:remove')")
    @Log(title = "人员证件信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicCardService.deleteBasicCardByIds(ids));
    }
}
