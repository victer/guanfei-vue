package com.ruoyi.guanfei.controller;

import java.util.List;

import com.ruoyi.guanfei.domain.BasicField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicContractTemplate;
import com.ruoyi.guanfei.service.IBasicContractTemplateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 合同模板信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Api(tags = "合同模板信息")
@RestController
@RequestMapping("/guanfei/basic/template")
public class BasicContractTemplateController extends BaseController
{
    @Autowired
    private IBasicContractTemplateService basicContractTemplateService;

    /**
     * 查询合同模板信息列表
     */
    @ApiOperation("查询合同模板信息列表")
    @PreAuthorize("@ss.hasPermi('guanfei:template:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicContractTemplate basicContractTemplate)
    {
        startPage();
        List<BasicContractTemplate> list = basicContractTemplateService.selectBasicContractTemplateList(basicContractTemplate);
        return getDataTable(list);
    }

    /**
     * 导出合同模板信息列表
     */
    @PreAuthorize("@ss.hasPermi('guanfei:template:export')")
    @Log(title = "合同模板信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicContractTemplate basicContractTemplate)
    {
        List<BasicContractTemplate> list = basicContractTemplateService.selectBasicContractTemplateList(basicContractTemplate);
        ExcelUtil<BasicContractTemplate> util = new ExcelUtil<BasicContractTemplate>(BasicContractTemplate.class);
        return util.exportExcel(list, "合同模板信息数据");
    }

    /**
     * 获取合同模板信息详细信息
     */
    @ApiOperation("获取合同模板信息详细信息")
    @ApiImplicitParam(name = "id", value = "获取合同模板信息详细信息", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:template:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicContractTemplateService.selectBasicContractTemplateById(id));
    }

    /**
     * 新增合同模板信息
     */
    @ApiOperation("新增合同模板信息")
    @ApiImplicitParam(name = "basicContractTemplate", value = "新增合同模板信息", dataType = "BasicContractTemplate")
    @PreAuthorize("@ss.hasPermi('guanfei:template:add')")
    @Log(title = "合同模板信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicContractTemplate basicContractTemplate)
    {
        return toAjax(basicContractTemplateService.insertBasicContractTemplate(basicContractTemplate));
    }

    /**
     * 修改合同模板信息
     */
    @ApiOperation("修改合同模板信息")
    @ApiImplicitParam(name = "basicContractTemplate", value = "修改合同模板信息", dataType = "BasicContractTemplate")
    @PreAuthorize("@ss.hasPermi('guanfei:template:edit')")
    @Log(title = "合同模板信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicContractTemplate basicContractTemplate)
    {
        return toAjax(basicContractTemplateService.updateBasicContractTemplate(basicContractTemplate));
    }

    /**
     * 删除合同模板信息
     */
    @ApiOperation("删除合同模板信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:template:remove')")
    @Log(title = "合同模板信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicContractTemplateService.deleteBasicContractTemplateByIds(ids));
    }

    /**
     * 查询合同模板信息列表(不分页)
     */
    @ApiOperation("查询田块信息列表(不分页,select选择使用)")
    @PreAuthorize("@ss.hasPermi('guanfei:template:edit')")
    @GetMapping("/listNoPaging")
    public AjaxResult listNoPaging()
    {
        List<BasicContractTemplate>  templates = basicContractTemplateService.listNoPaging();
        AjaxResult ajaxResult = AjaxResult.success();
        ajaxResult.put("templates",templates);
        return ajaxResult;
    }
}
