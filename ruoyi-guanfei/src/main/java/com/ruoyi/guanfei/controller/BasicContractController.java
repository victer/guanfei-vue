package com.ruoyi.guanfei.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.guanfei.domain.BasicContract;
import com.ruoyi.guanfei.service.IBasicContractService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 合同信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Api(tags = "合同信息管理")
@RestController
@RequestMapping("/guanfei/basic/contract")
public class BasicContractController extends BaseController
{
    @Autowired
    private IBasicContractService basicContractService;

    /**
     * 查询合同信息列表
     */
    @ApiOperation("查询人员证件信息列表")
    @PreAuthorize("@ss.hasPermi('guanfei:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicContract basicContract)
    {
        startPage();
        List<BasicContract> list = basicContractService.selectBasicContractList(basicContract);
        return getDataTable(list);
    }

    /**
     * 导出合同信息列表
     */

    @PreAuthorize("@ss.hasPermi('guanfei:contract:export')")
    @Log(title = "合同信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicContract basicContract)
    {
        List<BasicContract> list = basicContractService.selectBasicContractList(basicContract);
        ExcelUtil<BasicContract> util = new ExcelUtil<BasicContract>(BasicContract.class);
        return util.exportExcel(list, "合同信息数据");
    }

    /**
     * 获取合同信息详细信息
     */
    @ApiOperation("获取合同信息详细信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:contract:query')")
    @GetMapping(value = "/getOne/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(basicContractService.selectBasicContractById(id));
    }

    /**
     * 新增合同信息
     */
    @ApiOperation("新增合同信息")
    @ApiImplicitParam(name = "basicContract", value = "新增合同信息", dataType = "BasicContract")
    @PreAuthorize("@ss.hasPermi('guanfei:contract:add')")
    @Log(title = "合同信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody BasicContract basicContract)
    {
        return toAjax(basicContractService.insertBasicContract(basicContract));
    }

    /**
     * 修改合同信息
     */
    @ApiOperation("修改合同信息")
    @ApiImplicitParam(name = "basicContract", value = "修改人员证件信息", dataType = "BasicContract")
    @PreAuthorize("@ss.hasPermi('guanfei:contract:edit')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody BasicContract basicContract)
    {
        return toAjax(basicContractService.updateBasicContract(basicContract));
    }

    /**
     * 删除合同信息
     */
    @ApiOperation("删除合同信息")
    @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", paramType = "path")
    @PreAuthorize("@ss.hasPermi('guanfei:contract:remove')")
    @Log(title = "合同信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(basicContractService.deleteBasicContractByIds(ids));
    }
}
