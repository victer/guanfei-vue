package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.ShopCouponIssueUser;
import com.ruoyi.guanfei.service.IShopCouponIssueUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 优惠券领取Controller
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Api(tags = "优惠券领取")
@RestController
@RequestMapping("/guanfei/basic/issue")
public class ShopCouponIssueUserController extends BaseController
{
    @Autowired
    private IShopCouponIssueUserService shopCouponIssueUserService;

    /**
     * 查询优惠券领取列表
     */
    @ApiOperation("查询优惠券领取列表")
    @PreAuthorize("@ss.hasPermi('basic:issue:list')")
    @GetMapping("/list")
    public TableDataInfo list(ShopCouponIssueUser shopCouponIssueUser)
    {
        startPage();
        List<ShopCouponIssueUser> list = shopCouponIssueUserService.selectShopCouponIssueUserList(shopCouponIssueUser);
        return getDataTable(list);
    }

    /**
     * 导出优惠券领取列表
     */
    @ApiOperation("导出优惠券领取列表")
    @PreAuthorize("@ss.hasPermi('basic:issue:export')")
    @Log(title = "优惠券领取", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ShopCouponIssueUser shopCouponIssueUser)
    {
        List<ShopCouponIssueUser> list = shopCouponIssueUserService.selectShopCouponIssueUserList(shopCouponIssueUser);
        ExcelUtil<ShopCouponIssueUser> util = new ExcelUtil<ShopCouponIssueUser>(ShopCouponIssueUser.class);
        return util.exportExcel(list, "优惠券领取数据");
    }

    /**
     * 获取优惠券领取详细信息
     */
    @ApiOperation("获取优惠券领取详细信息")
    @PreAuthorize("@ss.hasPermi('basic:issue:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(shopCouponIssueUserService.selectShopCouponIssueUserById(id));
    }

    /**
     * 新增优惠券领取
     */
    @ApiOperation("新增优惠券领取")
    @PreAuthorize("@ss.hasPermi('basic:issue:add')")
    @Log(title = "优惠券领取", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ShopCouponIssueUser shopCouponIssueUser)
    {
        return toAjax(shopCouponIssueUserService.insertShopCouponIssueUser(shopCouponIssueUser));
    }

    /**
     * 修改优惠券领取
     */
    @ApiOperation("修改优惠券领取")
    @PreAuthorize("@ss.hasPermi('basic:issue:edit')")
    @Log(title = "优惠券领取", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody ShopCouponIssueUser shopCouponIssueUser)
    {
        return toAjax(shopCouponIssueUserService.updateShopCouponIssueUser(shopCouponIssueUser));
    }

    /**
     * 删除优惠券领取
     */
    @ApiOperation("删除优惠券领取")
    @PreAuthorize("@ss.hasPermi('basic:issue:remove')")
    @Log(title = "优惠券领取", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shopCouponIssueUserService.deleteShopCouponIssueUserByIds(ids));
    }
}
