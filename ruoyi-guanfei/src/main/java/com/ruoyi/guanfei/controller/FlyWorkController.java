package com.ruoyi.guanfei.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.guanfei.domain.FlyWork;
import com.ruoyi.guanfei.service.IFlyWorkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 无人机作业记录Controller
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Api(tags = "无人机作业记录")
@RestController
@RequestMapping("/guanfei/basic/work")
public class FlyWorkController extends BaseController
{
    @Autowired
    private IFlyWorkService flyWorkService;

    /**
     * 查询无人机作业记录列表
     */
    @ApiOperation("查询无人机作业记录列表")
    @PreAuthorize("@ss.hasPermi('basic:work:list')")
    @GetMapping("/list")
    public TableDataInfo list(FlyWork flyWork)
    {
        startPage();
        List<FlyWork> list = flyWorkService.selectFlyWorkList(flyWork);
        return getDataTable(list);
    }

    /**
     * 导出无人机作业记录列表
     */
    @ApiOperation("导出无人机作业记录列表")
    @PreAuthorize("@ss.hasPermi('basic:work:export')")
    @Log(title = "无人机作业记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(FlyWork flyWork)
    {
        List<FlyWork> list = flyWorkService.selectFlyWorkList(flyWork);
        ExcelUtil<FlyWork> util = new ExcelUtil<FlyWork>(FlyWork.class);
        return util.exportExcel(list, "无人机作业记录数据");
    }

    /**
     * 获取无人机作业记录详细信息
     */
    @ApiOperation("获取无人机作业记录详细信息")
    @PreAuthorize("@ss.hasPermi('basic:work:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(flyWorkService.selectFlyWorkById(id));
    }

    /**
     * 新增无人机作业记录
     */
    @ApiOperation("新增无人机作业记录")
    @PreAuthorize("@ss.hasPermi('basic:work:add')")
    @Log(title = "无人机作业记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody FlyWork flyWork)
    {
        return toAjax(flyWorkService.insertFlyWork(flyWork));
    }

    /**
     * 修改无人机作业记录
     */
    @ApiOperation("修改无人机作业记录")
    @PreAuthorize("@ss.hasPermi('basic:work:edit')")
    @Log(title = "无人机作业记录", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    public AjaxResult edit(@RequestBody FlyWork flyWork)
    {
        return toAjax(flyWorkService.updateFlyWork(flyWork));
    }

    /**
     * 删除无人机作业记录
     */
    @ApiOperation("删除无人机作业记录")
    @PreAuthorize("@ss.hasPermi('basic:work:remove')")
    @Log(title = "无人机作业记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(flyWorkService.deleteFlyWorkByIds(ids));
    }
}
