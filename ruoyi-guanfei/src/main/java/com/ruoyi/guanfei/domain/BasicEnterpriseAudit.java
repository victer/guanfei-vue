package com.ruoyi.guanfei.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 农资企业认证信息对象 basic_enterprise_audit
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Data
@ApiModel(value = "BasicEnterpriseAudit",description = "农资企业认证信息对象")
public class BasicEnterpriseAudit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 证件名称 */
    @ApiModelProperty("证件名称")
    @Excel(name = "证件名称")
    private String name;

    /** 证件编号 */
    @ApiModelProperty("证件编号")
    @Excel(name = "证件编号")
    private String code;

    /** 证件照片 */
    @ApiModelProperty("证件照片")
    @Excel(name = "证件照片")
    private Long image;

    /** 证件过期时间 */
    @ApiModelProperty("证件过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "证件过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 证件状态 0审核中 1正常 2失效 3异常 */
    @ApiModelProperty("证件状态 0审核中 1正常 2失效 3异常")
    @Excel(name = "证件状态 0审核中 1正常 2失效 3异常")
    private Long status;

    /** 农资企业ID */
    @ApiModelProperty("农资企业ID")
    @Excel(name = "农资企业ID")
    private Long enterpriseId;

    /* 字段关联 */
    /* 农资企业信息名称 */
    @ApiModelProperty("农资企业信息名称")
    private String enterpriseName;
}
