package com.ruoyi.guanfei.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同信息对象 basic_contract
 *
 * @author ruoyi
 * @date 2021-06-25
 */
@ApiModel(value = "BasicContract", description = "合同信息对象")
@Data
public class BasicContract extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private Long id;

    /**
     * 合同名称
     */
    @ApiModelProperty("合同名称")
    @Excel(name = "合同名称")
    private String name;

    /**
     * 合同编号
     */
    @ApiModelProperty("合同编号")
    @Excel(name = "合同编号")
    private String code;

    /**
     * 合同内容
     */
    @ApiModelProperty("合同内容")
    @Excel(name = "合同内容")
    private String content;

    /**
     * 合同生效截止时间
     */
    @ApiModelProperty("合同生效截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同生效截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /**
     * 合同甲方（农田拥有者）
     */
    @ApiModelProperty("合同甲方（农田拥有者）")
    @Excel(name = "合同甲方", readConverterExp = "农=田拥有者")
    private Long peopleIdOne;

    /**
     * 合同乙方（农机手或其他承包任务的单位）
     */
    @ApiModelProperty("合同乙方（农机手或其他承包任务的单位）")
    @Excel(name = "合同乙方", readConverterExp = "农=机手或其他承包任务的单位")
    private Long peopleIdTwo;

    /**
     * 合同中的田块
     */
    @ApiModelProperty("合同中的田块")
    @Excel(name = "合同中的田块")
    private Long fieldId;

    /**
     * 当前合同使用的模板ID
     */
    @ApiModelProperty("当前合同使用的模板ID")
    @Excel(name = "当前合同使用的模板ID")
    private Long templateId;

    /**
     * 合同参数填写完毕后根据模板文件生成的最终合同文件ID
     */
    @ApiModelProperty("合同参数填写完毕后根据模板文件生成的最终合同文件ID")
    @Excel(name = "合同参数填写完毕后根据模板文件生成的最终合同文件ID")
    private Long ossId;

    /*字段关联*/
    /* 甲方人员名称 */
    @ApiModelProperty("甲方人员名称")
    private String peopleOneName;
    /* 乙方人员名称 */
    @ApiModelProperty("乙方人员名称")
    private String peopleTwoName;
    /* 田块名称 */
    @ApiModelProperty("田块名称")
    private String fieldName;
    /* 模板名称 */
    @ApiModelProperty("模板名称")
    private String templateName;
}
