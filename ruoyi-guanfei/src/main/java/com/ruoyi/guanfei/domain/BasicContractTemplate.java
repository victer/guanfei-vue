package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同模板信息对象 basic_contract_template
 * 
 * @author ruoyi
 * @date 2021-06-25
 */
@Data
@ApiModel(value = "BasicContractTemplate",description = "合同模板信息对象")
public class BasicContractTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 合同模板名称 */
    @ApiModelProperty("合同模板名称")
    @Excel(name = "合同模板名称")
    private String name;

    /** 合同模板的类型 1.默认 */
    @ApiModelProperty("合同模板的类型 1.默认")
    @Excel(name = "合同模板的类型 1.默认")
    private Integer type;

    /** 合同模板预览图 */
    @ApiModelProperty("合同模板预览图")
    @Excel(name = "合同模板预览图")
    private String image;

    /** 合同内容 */
    @ApiModelProperty("合同内容")
    @Excel(name = "合同内容")
    private String content;

    /** 模板文件ID */
    @ApiModelProperty("模板文件ID")
    @Excel(name = "模板文件ID")
    private Long ossId;
}
