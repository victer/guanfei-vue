package com.ruoyi.guanfei.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 人员证件信息对象 basic_card
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@ApiModel(value = "BasicCard",description = "人员证件信息对象")
public class BasicCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 证件名称 */
    @ApiModelProperty("证件名称")
    @Excel(name = "证件名称")
    private String name;

    /** 证件编号 */
    @ApiModelProperty("证件编号")
    @Excel(name = "证件编号")
    private String code;

    /** 证件照片 */
    @ApiModelProperty("证件照片")
    @Excel(name = "证件照片")
    private Long image;

    /** 过期时间 */
    @ApiModelProperty("过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 证件状态 0审核中 1正常 2失效 3异常 */
    @ApiModelProperty("证件状态 0审核中 1正常 2失效 3异常")
    @Excel(name = "证件状态 0审核中 1正常 2失效 3异常")
    private Long status;

    /** 所属人员id */
    @ApiModelProperty("所属人员id")
    @Excel(name = "所属人员id")
    private Long peopleId;

    /** 农机id */
    @ApiModelProperty("农机id")
    @Excel(name = "农机id")
    private Long farmId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setImage(Long image) 
    {
        this.image = image;
    }

    public Long getImage() 
    {
        return image;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setPeopleId(Long peopleId) 
    {
        this.peopleId = peopleId;
    }

    public Long getPeopleId() 
    {
        return peopleId;
    }
    public void setFarmId(Long farmId) 
    {
        this.farmId = farmId;
    }

    public Long getFarmId() 
    {
        return farmId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("image", getImage())
            .append("endTime", getEndTime())
            .append("status", getStatus())
            .append("peopleId", getPeopleId())
            .append("farmId", getFarmId())
            .toString();
    }

    public BasicCard() {
    }

    public BasicCard(Long id, String name, String code, Long image, Date endTime, Long status, Long peopleId, Long farmId) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.image = image;
        this.endTime = endTime;
        this.status = status;
        this.peopleId = peopleId;
        this.farmId = farmId;
    }
}
