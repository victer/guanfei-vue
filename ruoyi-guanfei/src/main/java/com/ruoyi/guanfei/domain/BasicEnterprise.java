package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 农资企业信息对象 basic_enterprise
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "BasicEnterprise",description = "农资企业信息对象")
public class BasicEnterprise extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 企业名称 */
    @ApiModelProperty("企业名称")
    @Excel(name = "企业名称")
    private String name;

    /** 企业代码 */
    @ApiModelProperty("企业代码")
    @Excel(name = "企业代码")
    private String code;

    /** 联系人姓名 */
    @ApiModelProperty("联系人姓名")
    @Excel(name = "联系人姓名")
    private String peopleName;

    /** 联系人电话 */
    @ApiModelProperty("联系人电话")
    @Excel(name = "联系人电话")
    private String peoplePhone;

    /** 企业简介支持富文本 */
    @ApiModelProperty("企业简介支持富文本")
    @Excel(name = "企业简介支持富文本")
    private String introduce;

    /** 营业执照图片 */
    @ApiModelProperty("营业执照图片")
    @Excel(name = "营业执照图片")
    private Long license;

    /** 相关资质 */
    @ApiModelProperty("相关资质")
    @Excel(name = "相关资质")
    private String intelligence;

    /** 企业logo照片 */
    @ApiModelProperty("企业logo照片")
    @Excel(name = "企业logo照片")
    private Long logo;

    /** 经营范围 */
    @ApiModelProperty("经营范围")
    @Excel(name = "经营范围")
    private String range;

    /** 状态 0:审核中 1:正常 2:异常 */
    @ApiModelProperty("状态 0:审核中 1:正常 2:异常")
    @Excel(name = "状态 0:审核中 1:正常 2:异常")
    private Integer status;
}
