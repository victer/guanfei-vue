package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 人员信息对象 basic_people
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "BasicPeople",description = "人员信息对象")
public class BasicPeople extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 姓名 */
    @ApiModelProperty("姓名")
    @Excel(name = "姓名")
    private String name;

    /** 联系电话 */
    @ApiModelProperty("联系电话")
    @Excel(name = "联系电话")
    private Long phone;

    /** 身份证号码 */
    @ApiModelProperty("身份证号码")
    @Excel(name = "身份证号码")
    private String card;

    /** 身份证正面照片 */
    @ApiModelProperty("身份证正面照片")
    @Excel(name = "身份证正面照片")
    private Long cardJustImg;

    /** 身份证反面照片 */
    @ApiModelProperty("身份证反面照片")
    @Excel(name = "身份证反面照片")
    private Long cardBackImg;

    /** 人员类型 */
    @ApiModelProperty("人员类型")
    @Excel(name = "人员类型")
    private Long type;

    /** 昵称 */
    @ApiModelProperty("昵称")
    @Excel(name = "昵称")
    private String nickName;

    /** 登录密码 */
    @ApiModelProperty("登录密码")
    @Excel(name = "登录密码")
    private String password;

    /** 所属组织 */
    @ApiModelProperty("所属组织")
    @Excel(name = "所属组织")
    private Long companyId;

    /** 性别 */
    @ApiModelProperty("性别")
    @Excel(name = "性别")
    private Long gender;

    /** 备注 */
    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remarks;

    /** 微信ID */
    @ApiModelProperty("微信ID")
    @Excel(name = "微信ID")
    private String wechatId;

    /* 关联字段 */
    /*部门名称*/
    @ApiModelProperty("部门名称")
    private String companyName;
}
