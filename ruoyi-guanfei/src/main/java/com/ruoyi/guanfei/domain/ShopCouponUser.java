package com.ruoyi.guanfei.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 优惠券发放记录对象 shop_coupon_user
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ShopCouponUser",description = "优惠券发放记录对象")
public class ShopCouponUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 兑换的项目id */
    @ApiModelProperty("兑换的项目id")
    @Excel(name = "兑换的项目id")
    private Long oid;

    /** 兑换的人员id */
    @ApiModelProperty("兑换的人员id")
    @Excel(name = "兑换的人员id")
    private Long uid;

    /** 兑换的优惠券id */
    @ApiModelProperty("兑换的优惠券id")
    @Excel(name = "兑换的优惠券id")
    private Long cid;

    /** 优惠券使用时间 */
    @ApiModelProperty("优惠券使用时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "优惠券使用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date useTime;

    /** 优惠券获取方式 */
    @ApiModelProperty("优惠券获取方式")
    @Excel(name = "优惠券获取方式")
    private String type;

    /** 状态 0未使用 1已使用 2已过期 */
    @ApiModelProperty("状态 0未使用 1已使用 2已过期")
    @Excel(name = "状态 0未使用 1已使用 2已过期")
    private Integer status;

    /** 是否有效 0生效 1失效 */
    @ApiModelProperty("是否有效 0生效 1失效")
    @Excel(name = "是否有效 0生效 1失效")
    private Integer isFail;
}
