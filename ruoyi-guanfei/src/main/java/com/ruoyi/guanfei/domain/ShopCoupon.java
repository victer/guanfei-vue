package com.ruoyi.guanfei.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 优惠券对象 shop_coupon
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ShopCoupon",description = "优惠券对象")
public class ShopCoupon extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 优惠券标题 */
    @ApiModelProperty("优惠券标题")
    @Excel(name = "优惠券标题")
    private String title;

    /** 兑换消耗积分值 0为免费领券 */
    @ApiModelProperty("兑换消耗积分值 0为免费领券")
    @Excel(name = "兑换消耗积分值 0为免费领券")
    private Long integral;

    /** 优惠券面值 */
    @ApiModelProperty("优惠券面值")
    @Excel(name = "优惠券面值")
    private BigDecimal couponPrice;

    /** 最低消费多少金额可用优惠券 0为不限 */
    @ApiModelProperty("最低消费多少金额可用优惠券 0为不限 ")
    @Excel(name = "最低消费多少金额可用优惠券 0为不限")
    private BigDecimal useMinPrice;

    /** 优惠券有效期限（单位：天） 0为不限 */
    @ApiModelProperty("优惠券有效期限（单位：天） 0为不限")
    @Excel(name = "优惠券有效期限", readConverterExp = "单=位：天")
    private Long couponTime;

    /** 排序 */
    @ApiModelProperty("排序")
    @Excel(name = "排序")
    private Long sort;

    /** 状态 0未开启 1正常 2已无效 */
    @ApiModelProperty("状态 0未开启 1正常 2已无效")
    @Excel(name = "状态 0未开启 1正常 2已无效")
    private Integer status;

    /** 是否删除 0正常 1删除 */
    @ApiModelProperty("是否删除 0正常 1删除")
    @Excel(name = "是否删除 0正常 1删除")
    private Integer isDel;

    /** 所属商品id，有则代表和商品专用 */
    @ApiModelProperty("所属商品id，有则代表和商品专用")
    @Excel(name = "所属商品id，有则代表和商品专用")
    private Long productId;

    /** 优惠券分类id */
    @ApiModelProperty("优惠券分类id")
    @Excel(name = "优惠券分类id")
    private Long categoryId;

    /** 优惠券类型 0-通用 1-品类券 2-商品券 */
    @ApiModelProperty("优惠券类型 0-通用 1-品类券 2-商品券")
    @Excel(name = "优惠券类型 0-通用 1-品类券 2-商品券")
    private Integer type;

    /** 优惠券领取开启时间 */
    @ApiModelProperty("优惠券领取开启时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "优惠券领取开启时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 优惠券领取结束时间 */
    @ApiModelProperty("优惠券领取结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "优惠券领取结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 优惠券领取数量 */
    @ApiModelProperty("优惠券领取数量")
    @Excel(name = "优惠券领取数量")
    private Long totalCount;

    /** 优惠券剩余领取数量 */
    @ApiModelProperty("优惠券剩余领取数量")
    @Excel(name = "优惠券剩余领取数量")
    private Long remainCount;

    /** 是否无限张数 0不是 1是 */
    @ApiModelProperty("是否无限张数 0不是 1是 ")
    @Excel(name = "是否无限张数 0不是 1是")
    private Integer isPermanent;

    /** 是否首次注册赠送 0否(默认) 1是 */
    @ApiModelProperty("是否首次注册赠送 0否(默认) 1是 ")
    @Excel(name = "是否首次注册赠送 0否(默认) 1是")
    private Integer isRegisterSubscribe;

    /** 是否首次关注赠送 0否(默认) 1是 公众号等 */
    @ApiModelProperty("是否首次关注赠送 0否(默认) 1是 公众号等")
    @Excel(name = "是否首次关注赠送 0否(默认) 1是 公众号等")
    private Integer isGiveSubscribe;

    /** 是否满赠 0否(默认) 1是 */
    @ApiModelProperty("是否满赠 0否(默认) 1是")
    @Excel(name = "是否满赠 0否(默认) 1是")
    private Integer isFullGive;

    /** 消费满多少赠送优惠券 */
    @ApiModelProperty("消费满多少赠送优惠券")
    @Excel(name = "消费满多少赠送优惠券")
    private BigDecimal fullReduction;
}
