package com.ruoyi.guanfei.domain;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 农资产品信息对象 basic_goods
 * 
 * @author ruoyi
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "BasicGoods",description = "农资产品信息对象")
public class BasicGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 商品名称 */
    @ApiModelProperty("商品名称")
    @Excel(name = "商品名称")
    private String name;

    /** 商品类型 */
    @ApiModelProperty("商品类型")
    @Excel(name = "商品类型")
    private Long type;

    /** 商品品牌 */
    @ApiModelProperty("商品品牌")
    @Excel(name = "商品品牌")
    private String brand;

    /** 原价 */
    @ApiModelProperty("原价")
    @Excel(name = "原价")
    private BigDecimal primaryPrice;

    /** 现价 */
    @ApiModelProperty("现价")
    @Excel(name = "现价")
    private BigDecimal nowPrice;

    /** 农资企业id */
    @ApiModelProperty("农资企业id")
    @Excel(name = "农资企业id")
    private Long enterpriseId;

    /** 商品简介 */
    @ApiModelProperty("商品简介")
    @Excel(name = "商品简介")
    private String introduce;
}
