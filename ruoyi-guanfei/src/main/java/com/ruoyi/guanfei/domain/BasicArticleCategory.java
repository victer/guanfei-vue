package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资讯信息分类对象 basic_article_category
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Data
public class BasicArticleCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 文章分类标题 */
    @ApiModelProperty("文章分类标题")
    @Excel(name = "文章分类标题")
    private String title;

    /** 文章分类简介 */
    @ApiModelProperty("文章分类简介")
    @Excel(name = "文章分类简介")
    private String intr;

    /** 文章分类状态 0正常 1禁用 */
    @ApiModelProperty("文章分类状态 0正常 1禁用")
    @Excel(name = "文章分类状态 0正常 1禁用")
    private Long status;

    /** 文章分类排序 */
    @ApiModelProperty("文章分类排序")
    @Excel(name = "文章分类排序")
    private Long sort;
}
