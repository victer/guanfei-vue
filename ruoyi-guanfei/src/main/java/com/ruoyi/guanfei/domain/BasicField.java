package com.ruoyi.guanfei.domain;

import java.math.BigDecimal;

import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 田块信息对象 basic_field
 *
 * @author ruoyi
 * @date 2021-06-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "BasicField",description = "田块信息对象")
public class BasicField extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 户主姓名 */
    @ApiModelProperty("户主姓名")
    @Excel(name = "户主姓名")
    private String name;

    /** 户主身份证号码 */
    @ApiModelProperty("户主身份证号码")
    @Excel(name = "户主身份证号码")
    private String card;

    /** 田块面积(亩) */
    @ApiModelProperty("田块面积(亩)")
    @Excel(name = "田块面积(亩)")
    private BigDecimal area;

    /** 联系电话 */
    @ApiModelProperty("联系电话")
    @Excel(name = "联系电话")
    private Long phone;

    /** 组织主键 */
    @ApiModelProperty("组织主键")
    @Excel(name = "组织主键")
    private Long companyId;

    /** 部门对象 */
    @Excels({
            @Excel(name = "组织名称", targetAttr = "deptName", type = Excel.Type.EXPORT),
            @Excel(name = "组织负责人", targetAttr = "leader", type = Excel.Type.EXPORT)
    })
    private SysDept dept;

    /** 田块性质 */
    @ApiModelProperty("田块性质")
    @Excel(name = "田块性质")
    private String nature;

    /** 备注 */
    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remarks;
}
