package com.ruoyi.guanfei.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 无人机作业记录对象 fly_work
 *
 * @author ruoyi
 * @date 2021-06-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "FlyWork",description = "无人机作业记录对象")
public class FlyWork extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty("主键")
    private Long id;

    /**
     * 无人机设备主键
     */
    @ApiModelProperty("无人机设备主键")
    @Excel(name = "无人机设备主键")
    private Long devId;

    /**
     * 作业开始时间
     */
    @ApiModelProperty("作业开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "作业开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date flyStartTs;

    /**
     * 作业结束时间
     */
    @ApiModelProperty("作业结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "作业结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date flyEndTs;

    /**
     * 有效作业面积
     */
    @ApiModelProperty("有效作业面积")
    @Excel(name = "有效作业面积")
    private BigDecimal area;

    /**
     * 作业时长(单位分钟)
     */
    @ApiModelProperty("作业时长(单位分钟)")
    @Excel(name = "作业时长(单位分钟)")
    private BigDecimal workTime;

    /**
     * 年月日如：20210604
     */
    @ApiModelProperty("年月日如：20210604")
    @Excel(name = "年月日如：20210604")
    private Long ymd;

    /**
     * 作业流水号一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业(sn在此表中唯一)
     */
    @ApiModelProperty("作业流水号一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业(sn在此表中唯一)")
    @Excel(name = "作业流水号一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业(sn在此表中唯一)")
    private String sn;

    /**
     * 作业人员id
     */
    @ApiModelProperty("作业人员id")
    @Excel(name = "作业人员id")
    private Long peopleId;

    /**
     * 订单id
     */
    @ApiModelProperty("订单id")
    @Excel(name = "订单id")
    private Long orderId;
}
