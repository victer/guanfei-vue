package com.ruoyi.guanfei.domain;

import com.ruoyi.common.annotation.Excels;
import com.ruoyi.common.core.domain.entity.SysDept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 农机信息对象 basic_farm
 *
 * @author ruoyi
 * @date 2021-06-17
 */
@ApiModel(value = "BasicFarm",description = "农机信息对象")
public class BasicFarm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 农机编号 */
    @ApiModelProperty("农机编号")
    @Excel(name = "农机编号")
    private String code;

    /** 农机标识 */
    @ApiModelProperty("农机标识")
    @Excel(name = "农机标识")
    private String sign;

    /** 农机类型 */
    @ApiModelProperty("农机类型")
    @Excel(name = "农机类型")
    private Long type;

    /** 所属人员 */
    @ApiModelProperty("所属人员")
    private Long peopleId;

    /** 农机状态 */
    @ApiModelProperty("农机状态")
    @Excel(name = "农机状态")
    private Long status;

    /** 挂靠组织 */
    @ApiModelProperty("挂靠组织")
    private Long companyId;

    /** 备注 */
    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remarks;

    /** 农机照片 */
    @ApiModelProperty("农机照片")
    @Excel(name = "农机照片")
    private String image;

    /** 部门对象 */
    @Excels({
            @Excel(name = "组织名称", targetAttr = "deptName", type = Excel.Type.EXPORT),
            @Excel(name = "组织负责人", targetAttr = "leader", type = Excel.Type.EXPORT)
    })
    private SysDept dept;

    @Excels({@Excel(name = "所属人员",targetAttr = "name", type = Excel.Type.EXPORT)})
    private BasicPeople people;

    public BasicPeople getPeople() {
        return people;
    }

    public void setPeople(BasicPeople people) {
        this.people = people;
    }

    public SysDept getDept() {
        return dept;
    }

    public void setDept(SysDept dept) {
        this.dept = dept;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setSign(String sign)
    {
        this.sign = sign;
    }

    public String getSign()
    {
        return sign;
    }
    public void setType(Long type)
    {
        this.type = type;
    }

    public Long getType()
    {
        return type;
    }
    public void setPeopleId(Long peopleId)
    {
        this.peopleId = peopleId;
    }

    public Long getPeopleId()
    {
        return peopleId;
    }
    public void setStatus(Long status)
    {
        this.status = status;
    }

    public Long getStatus()
    {
        return status;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }
    public void setImage(String image)
    {
        this.image = image;
    }

    public String getImage()
    {
        return image;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("code", getCode())
                .append("sign", getSign())
                .append("type", getType())
                .append("peopleId", getPeopleId())
                .append("status", getStatus())
                .append("companyId", getCompanyId())
                .append("remarks", getRemarks())
                .append("image", getImage())
                .append("dept", getDept())
                .append("people", getPeople())
                .toString();
    }

    public BasicFarm() {
    }

    public BasicFarm(Long id, String code, String sign, Long type, Long peopleId, Long status, Long companyId, String remarks, String image,SysDept dept,BasicPeople people) {
        this.id = id;
        this.code = code;
        this.sign = sign;
        this.type = type;
        this.peopleId = peopleId;
        this.status = status;
        this.companyId = companyId;
        this.remarks = remarks;
        this.image = image;
        this.dept = dept;
        this.people = people;
    }
}
