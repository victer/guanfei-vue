package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资讯信息对象 basic_article
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Data
public class BasicArticle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 文章分类id主键 */
    @ApiModelProperty("文章分类id主键")
    @Excel(name = "文章分类id主键")
    private Long cid;

    /** 文章标题 */
    @ApiModelProperty("文章标题")
    @Excel(name = "文章标题")
    private String title;

    /** 文章作者 */
    @ApiModelProperty("文章作者")
    @Excel(name = "文章作者")
    private String author;

    /** 文章图片主键 */
    @ApiModelProperty("文章图片主键")
    @Excel(name = "文章图片主键")
    private Long imageInput;

    /** 文章简介 */
    @ApiModelProperty("文章简介")
    @Excel(name = "文章简介")
    private String synopsis;

    /** 文章分享标题 */
    @ApiModelProperty("文章分享标题")
    @Excel(name = "文章分享标题")
    private String shareTitle;

    /** 文章分享简介 */
    @ApiModelProperty("文章分享简介")
    @Excel(name = "文章分享简介")
    private String shareSynopsis;

    /** 浏览次数 */
    @ApiModelProperty("浏览次数")
    @Excel(name = "浏览次数")
    private String visit;

    /** 排序 */
    @ApiModelProperty("排序")
    @Excel(name = "排序")
    private Long sort;

    /** 原文链接 */
    @ApiModelProperty("文章标题")
    @Excel(name = "原文链接")
    private String url;

    /** 文章状态 0正常 1禁用 */
    @ApiModelProperty("文章状态 0正常 1禁用")
    @Excel(name = "文章状态 0正常 1禁用")
    private Long status;

    /** 商品关联id主键 */
    @ApiModelProperty("商品关联id主键")
    @Excel(name = "商品关联id主键")
    private Long productId;

    /** 是否热门 0不是 1是 */
    @ApiModelProperty("是否热门 0不是 1是")
    @Excel(name = "是否热门 0不是 1是")
    private Long isHot;

    /** 是否是轮播图 0不是 1是 */
    @ApiModelProperty("是否是轮播图 0不是 1是")
    @Excel(name = "是否是轮播图 0不是 1是")
    private Long isBanner;

    /** 文章内容 */
    @ApiModelProperty("文章内容")
    @Excel(name = "文章内容")
    private String content;

    /*关联字段*/
    /*文章分类标题*/
    @ApiModelProperty("文章分类标题")
    private String cTitle;

    /*商品名称*/
    @ApiModelProperty("商品名称")
    private String productName;
}
