package com.ruoyi.guanfei.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 优惠券领取对象 shop_coupon_issue_user
 * 
 * @author ruoyi
 * @date 2021-06-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ShopCouponIssueUser",description = "优惠券领取对象")
public class ShopCouponIssueUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty("主键")
    private Long id;

    /** 人员id */
    @ApiModelProperty("人员id")
    @Excel(name = "人员id")
    private Long peopleId;

    /** 优惠券id */
    @ApiModelProperty("优惠券id")
    @Excel(name = "优惠券id")
    private Long couponId;
}
