package com.ruoyi.guanfei.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 无人机作业原始记录对象 fly_work_records
 * 
 * @author ruoyi
 * @date 2021-06-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "FlyWorkRecords",description = "无人机作业原始记录对象")
public class FlyWorkRecords extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 作业记录主键 */
    @ApiModelProperty("作业记录主键")
    private Long id;

    /** 时间戳 */
    @ApiModelProperty("时间戳")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间戳", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ts;

    /** 无人机设备主键 */
    @ApiModelProperty("无人机设备主键")
    @Excel(name = "无人机设备主键")
    private Long devId;

    /** 年月日20210612 */
    @ApiModelProperty("年月日20210612")
    @Excel(name = "年月日20210612")
    private Long ymd;

    /** 经度 */
    @ApiModelProperty("经度")
    @Excel(name = "经度")
    private BigDecimal longitude;

    /** 纬度 */
    @ApiModelProperty("纬度")
    @Excel(name = "纬度")
    private BigDecimal latitude;

    /** 处理标记0-未处理，1-已经处理 */
    @ApiModelProperty("处理标记0-未处理，1-已经处理")
    @Excel(name = "处理标记0-未处理，1-已经处理")
    private Long flag;

    /** 飞行速度 */
    @ApiModelProperty("飞行速度")
    @Excel(name = "飞行速度")
    private BigDecimal speed;

    /** 飞行高度 */
    @ApiModelProperty("飞行高度")
    @Excel(name = "飞行高度")
    private BigDecimal height;

    /** 喷头状态0-未喷洒，1-正常喷洒 */
    @ApiModelProperty("喷头状态0-未喷洒，1-正常喷洒")
    @Excel(name = "喷头状态0-未喷洒，1-正常喷洒")
    private Long nozzleStatus;

    /** 电池电量,取值范围为0~100 */
    @ApiModelProperty("电池电量,取值范围为0~100")
    @Excel(name = "电池电量,取值范围为0~100")
    private Long battery;

    /** 电流 */
    @ApiModelProperty("电流")
    @Excel(name = "电流")
    private BigDecimal electricCurrent;

    /** 作业流水号,一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业 （外键关联work_gis 表中的sn） */
    @ApiModelProperty("作业流水号,一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业 （外键关联work_gis 表中的sn）")
    @Excel(name = "作业流水号,一次起飞，一次降落的所有所有原始数据点集的sn一致，代表同一次作业 ", readConverterExp = "外=键关联work_gis,表=中的sn")
    private String sn;
}
