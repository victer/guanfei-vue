package com.ruoyi.sms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.sms.mapper.SysSmsChannelMapper;
import com.ruoyi.sms.domain.SysSmsChannel;
import com.ruoyi.sms.service.ISysSmsChannelService;

/**
 * 短信渠道Service业务层处理
 * 
 * @author ace
 * @date 2021-06-19
 */
@Service
public class SysSmsChannelServiceImpl implements ISysSmsChannelService 
{
    @Autowired
    private SysSmsChannelMapper sysSmsChannelMapper;

    /**
     * 查询短信渠道
     * 
     * @param id 短信渠道ID
     * @return 短信渠道
     */
    @Override
    public SysSmsChannel selectSysSmsChannelById(Long id)
    {
        return sysSmsChannelMapper.selectSysSmsChannelById(id);
    }

    /**
     * 查询短信渠道列表
     * 
     * @param sysSmsChannel 短信渠道
     * @return 短信渠道
     */
    @Override
    public List<SysSmsChannel> selectSysSmsChannelList(SysSmsChannel sysSmsChannel)
    {
        return sysSmsChannelMapper.selectSysSmsChannelList(sysSmsChannel);
    }

    /**
     * 新增短信渠道
     * 
     * @param sysSmsChannel 短信渠道
     * @return 结果
     */
    @Override
    public int insertSysSmsChannel(SysSmsChannel sysSmsChannel)
    {
        sysSmsChannel.setCreateTime(DateUtils.getNowDate());
        return sysSmsChannelMapper.insertSysSmsChannel(sysSmsChannel);
    }

    /**
     * 修改短信渠道
     * 
     * @param sysSmsChannel 短信渠道
     * @return 结果
     */
    @Override
    public int updateSysSmsChannel(SysSmsChannel sysSmsChannel)
    {
        sysSmsChannel.setUpdateTime(DateUtils.getNowDate());
        return sysSmsChannelMapper.updateSysSmsChannel(sysSmsChannel);
    }

    /**
     * 批量删除短信渠道
     * 
     * @param ids 需要删除的短信渠道ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsChannelByIds(Long[] ids)
    {
        return sysSmsChannelMapper.deleteSysSmsChannelByIds(ids);
    }

    /**
     * 删除短信渠道信息
     * 
     * @param id 短信渠道ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsChannelById(Long id)
    {
        return sysSmsChannelMapper.deleteSysSmsChannelById(id);
    }
}
