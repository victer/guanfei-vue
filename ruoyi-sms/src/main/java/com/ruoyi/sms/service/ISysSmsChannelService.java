package com.ruoyi.sms.service;

import java.util.List;
import com.ruoyi.sms.domain.SysSmsChannel;

/**
 * 短信渠道Service接口
 * 
 * @author ace
 * @date 2021-06-19
 */
public interface ISysSmsChannelService 
{
    /**
     * 查询短信渠道
     * 
     * @param id 短信渠道ID
     * @return 短信渠道
     */
    public SysSmsChannel selectSysSmsChannelById(Long id);

    /**
     * 查询短信渠道列表
     * 
     * @param sysSmsChannel 短信渠道
     * @return 短信渠道集合
     */
    public List<SysSmsChannel> selectSysSmsChannelList(SysSmsChannel sysSmsChannel);

    /**
     * 新增短信渠道
     * 
     * @param sysSmsChannel 短信渠道
     * @return 结果
     */
    public int insertSysSmsChannel(SysSmsChannel sysSmsChannel);

    /**
     * 修改短信渠道
     * 
     * @param sysSmsChannel 短信渠道
     * @return 结果
     */
    public int updateSysSmsChannel(SysSmsChannel sysSmsChannel);

    /**
     * 批量删除短信渠道
     * 
     * @param ids 需要删除的短信渠道ID
     * @return 结果
     */
    public int deleteSysSmsChannelByIds(Long[] ids);

    /**
     * 删除短信渠道信息
     * 
     * @param id 短信渠道ID
     * @return 结果
     */
    public int deleteSysSmsChannelById(Long id);
}
