package com.ruoyi.sms.service;

import java.util.List;
import com.ruoyi.sms.domain.SysSmsTemplate;

/**
 * 短信模板Service接口
 * 
 * @author ace
 * @date 2021-06-19
 */
public interface ISysSmsTemplateService 
{
    /**
     * 查询短信模板
     * 
     * @param id 短信模板ID
     * @return 短信模板
     */
    public SysSmsTemplate selectSysSmsTemplateById(Long id);

    /**
     * 查询短信模板列表
     * 
     * @param sysSmsTemplate 短信模板
     * @return 短信模板集合
     */
    public List<SysSmsTemplate> selectSysSmsTemplateList(SysSmsTemplate sysSmsTemplate);

    /**
     * 新增短信模板
     * 
     * @param sysSmsTemplate 短信模板
     * @return 结果
     */
    public int insertSysSmsTemplate(SysSmsTemplate sysSmsTemplate);

    /**
     * 修改短信模板
     * 
     * @param sysSmsTemplate 短信模板
     * @return 结果
     */
    public int updateSysSmsTemplate(SysSmsTemplate sysSmsTemplate);

    /**
     * 批量删除短信模板
     * 
     * @param ids 需要删除的短信模板ID
     * @return 结果
     */
    public int deleteSysSmsTemplateByIds(Long[] ids);

    /**
     * 删除短信模板信息
     * 
     * @param id 短信模板ID
     * @return 结果
     */
    public int deleteSysSmsTemplateById(Long id);
}
