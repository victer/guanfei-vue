package com.ruoyi.sms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.sms.mapper.SysSmsLogMapper;
import com.ruoyi.sms.domain.SysSmsLog;
import com.ruoyi.sms.service.ISysSmsLogService;

/**
 * 短信日志Service业务层处理
 * 
 * @author ace
 * @date 2021-06-19
 */
@Service
public class SysSmsLogServiceImpl implements ISysSmsLogService 
{
    @Autowired
    private SysSmsLogMapper sysSmsLogMapper;

    /**
     * 查询短信日志
     * 
     * @param id 短信日志ID
     * @return 短信日志
     */
    @Override
    public SysSmsLog selectSysSmsLogById(Long id)
    {
        return sysSmsLogMapper.selectSysSmsLogById(id);
    }

    /**
     * 查询短信日志列表
     * 
     * @param sysSmsLog 短信日志
     * @return 短信日志
     */
    @Override
    public List<SysSmsLog> selectSysSmsLogList(SysSmsLog sysSmsLog)
    {
        return sysSmsLogMapper.selectSysSmsLogList(sysSmsLog);
    }

    /**
     * 新增短信日志
     * 
     * @param sysSmsLog 短信日志
     * @return 结果
     */
    @Override
    public int insertSysSmsLog(SysSmsLog sysSmsLog)
    {
        sysSmsLog.setCreateTime(DateUtils.getNowDate());
        return sysSmsLogMapper.insertSysSmsLog(sysSmsLog);
    }

    /**
     * 修改短信日志
     * 
     * @param sysSmsLog 短信日志
     * @return 结果
     */
    @Override
    public int updateSysSmsLog(SysSmsLog sysSmsLog)
    {
        sysSmsLog.setUpdateTime(DateUtils.getNowDate());
        return sysSmsLogMapper.updateSysSmsLog(sysSmsLog);
    }

    /**
     * 批量删除短信日志
     * 
     * @param ids 需要删除的短信日志ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsLogByIds(Long[] ids)
    {
        return sysSmsLogMapper.deleteSysSmsLogByIds(ids);
    }

    /**
     * 删除短信日志信息
     * 
     * @param id 短信日志ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsLogById(Long id)
    {
        return sysSmsLogMapper.deleteSysSmsLogById(id);
    }
}
