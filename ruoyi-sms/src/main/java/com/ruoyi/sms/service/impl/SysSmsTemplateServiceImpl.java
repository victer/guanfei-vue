package com.ruoyi.sms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.sms.mapper.SysSmsTemplateMapper;
import com.ruoyi.sms.domain.SysSmsTemplate;
import com.ruoyi.sms.service.ISysSmsTemplateService;

/**
 * 短信模板Service业务层处理
 * 
 * @author ace
 * @date 2021-06-19
 */
@Service
public class SysSmsTemplateServiceImpl implements ISysSmsTemplateService 
{
    @Autowired
    private SysSmsTemplateMapper sysSmsTemplateMapper;

    /**
     * 查询短信模板
     * 
     * @param id 短信模板ID
     * @return 短信模板
     */
    @Override
    public SysSmsTemplate selectSysSmsTemplateById(Long id)
    {
        return sysSmsTemplateMapper.selectSysSmsTemplateById(id);
    }

    /**
     * 查询短信模板列表
     * 
     * @param sysSmsTemplate 短信模板
     * @return 短信模板
     */
    @Override
    public List<SysSmsTemplate> selectSysSmsTemplateList(SysSmsTemplate sysSmsTemplate)
    {
        return sysSmsTemplateMapper.selectSysSmsTemplateList(sysSmsTemplate);
    }

    /**
     * 新增短信模板
     * 
     * @param sysSmsTemplate 短信模板
     * @return 结果
     */
    @Override
    public int insertSysSmsTemplate(SysSmsTemplate sysSmsTemplate)
    {
        sysSmsTemplate.setCreateTime(DateUtils.getNowDate());
        return sysSmsTemplateMapper.insertSysSmsTemplate(sysSmsTemplate);
    }

    /**
     * 修改短信模板
     * 
     * @param sysSmsTemplate 短信模板
     * @return 结果
     */
    @Override
    public int updateSysSmsTemplate(SysSmsTemplate sysSmsTemplate)
    {
        sysSmsTemplate.setUpdateTime(DateUtils.getNowDate());
        return sysSmsTemplateMapper.updateSysSmsTemplate(sysSmsTemplate);
    }

    /**
     * 批量删除短信模板
     * 
     * @param ids 需要删除的短信模板ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsTemplateByIds(Long[] ids)
    {
        return sysSmsTemplateMapper.deleteSysSmsTemplateByIds(ids);
    }

    /**
     * 删除短信模板信息
     * 
     * @param id 短信模板ID
     * @return 结果
     */
    @Override
    public int deleteSysSmsTemplateById(Long id)
    {
        return sysSmsTemplateMapper.deleteSysSmsTemplateById(id);
    }
}
