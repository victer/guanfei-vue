package com.ruoyi.sms.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.sms.domain.SysSmsTemplate;
import com.ruoyi.sms.service.ISysSmsTemplateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 短信模板Controller
 * 
 * @author ace
 * @date 2021-06-19
 */
@RestController
@RequestMapping("/sms/template")
public class SysSmsTemplateController extends BaseController
{
    @Autowired
    private ISysSmsTemplateService sysSmsTemplateService;

    /**
     * 查询短信模板列表
     */
    @PreAuthorize("@ss.hasPermi('sms:template:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysSmsTemplate sysSmsTemplate)
    {
        startPage();
        List<SysSmsTemplate> list = sysSmsTemplateService.selectSysSmsTemplateList(sysSmsTemplate);
        return getDataTable(list);
    }

    /**
     * 导出短信模板列表
     */
    @PreAuthorize("@ss.hasPermi('sms:template:export')")
    @Log(title = "短信模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSmsTemplate sysSmsTemplate)
    {
        List<SysSmsTemplate> list = sysSmsTemplateService.selectSysSmsTemplateList(sysSmsTemplate);
        ExcelUtil<SysSmsTemplate> util = new ExcelUtil<SysSmsTemplate>(SysSmsTemplate.class);
        return util.exportExcel(list, "短信模板数据");
    }

    /**
     * 获取短信模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('sms:template:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysSmsTemplateService.selectSysSmsTemplateById(id));
    }

    /**
     * 新增短信模板
     */
    @PreAuthorize("@ss.hasPermi('sms:template:add')")
    @Log(title = "短信模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysSmsTemplate sysSmsTemplate)
    {
        return toAjax(sysSmsTemplateService.insertSysSmsTemplate(sysSmsTemplate));
    }

    /**
     * 修改短信模板
     */
    @PreAuthorize("@ss.hasPermi('sms:template:edit')")
    @Log(title = "短信模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysSmsTemplate sysSmsTemplate)
    {
        return toAjax(sysSmsTemplateService.updateSysSmsTemplate(sysSmsTemplate));
    }

    /**
     * 删除短信模板
     */
    @PreAuthorize("@ss.hasPermi('sms:template:remove')")
    @Log(title = "短信模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysSmsTemplateService.deleteSysSmsTemplateByIds(ids));
    }
}
