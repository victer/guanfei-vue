package com.ruoyi.sms.core.client;


import com.ruoyi.common.exception.ErrorCode;

import java.util.function.Function;

/**
 * 将 API 的错误码，转换为通用的错误码
 *
 * @see SmsCommonResult
 * @see com.ruoyi.sms.core.enums.SmsFrameworkErrorCodeConstants
 *
 * @author ace
 */
public interface SmsCodeMapping extends Function<String, ErrorCode> {
}
