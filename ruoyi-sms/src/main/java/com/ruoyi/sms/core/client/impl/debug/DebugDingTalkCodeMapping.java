package com.ruoyi.sms.core.client.impl.debug;


import com.ruoyi.common.enums.enums.GlobalErrorCodeConstants;
import com.ruoyi.common.exception.ErrorCode;
import com.ruoyi.sms.core.client.SmsCodeMapping;
import com.ruoyi.sms.core.enums.SmsFrameworkErrorCodeConstants;

import java.util.Objects;

/**
 * 钉钉的 SmsCodeMapping 实现类
 *
 * @author ace
 */
public class DebugDingTalkCodeMapping implements SmsCodeMapping {

    @Override
    public ErrorCode apply(String apiCode) {
        return Objects.equals(apiCode, "0") ? GlobalErrorCodeConstants.SUCCESS : SmsFrameworkErrorCodeConstants.SMS_UNKNOWN;
    }

}
