package com.ruoyi.sms.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 短信日志对象 sys_sms_log
 *
 * @author ace
 * @date 2021-06-19
 */
@Data
public class SysSmsLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 短信渠道编号
     */
    @Excel(name = "短信渠道编号")
    private Long channelId;

    /**
     * 短信渠道编码
     */
    @Excel(name = "短信渠道编码")
    private String channelCode;

    /**
     * 模板编号
     */
    @Excel(name = "模板编号")
    private Long templateId;

    /**
     * 模板编码
     */
    @Excel(name = "模板编码")
    private String templateCode;

    /**
     * 短信类型
     */
    @Excel(name = "短信类型")
    private Integer templateType;

    /**
     * 短信内容
     */
    @Excel(name = "短信内容")
    private String templateContent;

    /**
     * 短信参数
     */
    @Excel(name = "短信参数")
    private String templateParams;

    /**
     * 短信 API 的模板编号
     */
    @Excel(name = "短信 API 的模板编号")
    private String apiTemplateId;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String mobile;

    /**
     * 用户编号
     */
    @Excel(name = "用户编号")
    private Long userId;

    /**
     * 用户类型
     */
    @Excel(name = "用户类型")
    private Integer userType;

    /**
     * 发送状态
     */
    @Excel(name = "发送状态")
    private Integer sendStatus;

    /**
     * 发送时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendTime;

    /**
     * 发送结果的编码
     */
    @Excel(name = "发送结果的编码")
    private Long sendCode;

    /**
     * 发送结果的提示
     */
    @Excel(name = "发送结果的提示")
    private String sendMsg;

    /**
     * 短信 API 发送结果的编码
     */
    @Excel(name = "短信 API 发送结果的编码")
    private String apiSendCode;

    /**
     * 短信 API 发送失败的提示
     */
    @Excel(name = "短信 API 发送失败的提示")
    private String apiSendMsg;

    /**
     * 短信 API 发送返回的唯一请求 ID
     */
    @Excel(name = "短信 API 发送返回的唯一请求 ID")
    private String apiRequestId;

    /**
     * 短信 API 发送返回的序号
     */
    @Excel(name = "短信 API 发送返回的序号")
    private String apiSerialNo;

    /**
     * 接收状态
     */
    @Excel(name = "接收状态")
    private Integer receiveStatus;

    /**
     * 接收时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "接收时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date receiveTime;

    /**
     * API 接收结果的编码
     */
    @Excel(name = "API 接收结果的编码")
    private String apiReceiveCode;

    /**
     * API 接收结果的说明
     */
    @Excel(name = "API 接收结果的说明")
    private String apiReceiveMsg;

    /**
     * 创建者
     */
    @Excel(name = "创建者")
    private String creator;

    /**
     * 更新者
     */
    @Excel(name = "更新者")
    private String updater;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("channelId", getChannelId())
                .append("channelCode", getChannelCode())
                .append("templateId", getTemplateId())
                .append("templateCode", getTemplateCode())
                .append("templateType", getTemplateType())
                .append("templateContent", getTemplateContent())
                .append("templateParams", getTemplateParams())
                .append("apiTemplateId", getApiTemplateId())
                .append("mobile", getMobile())
                .append("userId", getUserId())
                .append("userType", getUserType())
                .append("sendStatus", getSendStatus())
                .append("sendTime", getSendTime())
                .append("sendCode", getSendCode())
                .append("sendMsg", getSendMsg())
                .append("apiSendCode", getApiSendCode())
                .append("apiSendMsg", getApiSendMsg())
                .append("apiRequestId", getApiRequestId())
                .append("apiSerialNo", getApiSerialNo())
                .append("receiveStatus", getReceiveStatus())
                .append("receiveTime", getReceiveTime())
                .append("apiReceiveCode", getApiReceiveCode())
                .append("apiReceiveMsg", getApiReceiveMsg())
                .append("creator", getCreator())
                .append("createTime", getCreateTime())
                .append("updater", getUpdater())
                .append("updateTime", getUpdateTime())
                .append("deleted", getDeleted())
                .toString();
    }
}
