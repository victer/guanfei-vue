package com.ruoyi.sms.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 短信模板对象 sys_sms_template
 *
 * @author ace
 * @date 2021-06-19
 */
@Data
public class SysSmsTemplate extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 短信签名
     */
    @Excel(name = "短信签名")
    private Integer type;

    /**
     * 开启状态
     */
    @Excel(name = "开启状态")
    private Integer status;

    /**
     * 模板编码
     */
    @Excel(name = "模板编码")
    private String code;

    /**
     * 模板名称
     */
    @Excel(name = "模板名称")
    private String name;

    /**
     * 模板内容
     */
    @Excel(name = "模板内容")
    private String content;

    /**
     * 参数数组
     */
    @Excel(name = "参数数组")
    private String param;

    /**
     * 短信 API 的模板编号
     */
    @Excel(name = "短信 API 的模板编号")
    private String apiTemplateId;

    /**
     * 短信渠道编号
     */
    @Excel(name = "短信渠道编号")
    private Long channelId;

    /**
     * 短信渠道编码
     */
    @Excel(name = "短信渠道编码")
    private String channelCode;

    /**
     * 创建者
     */
    @Excel(name = "创建者")
    private String creator;

    /**
     * 更新者
     */
    @Excel(name = "更新者")
    private String updater;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("type", getType())
                .append("status", getStatus())
                .append("code", getCode())
                .append("name", getName())
                .append("content", getContent())
                .append("params", getParams())
                .append("remark", getRemark())
                .append("apiTemplateId", getApiTemplateId())
                .append("channelId", getChannelId())
                .append("channelCode", getChannelCode())
                .append("creator", getCreator())
                .append("createTime", getCreateTime())
                .append("updater", getUpdater())
                .append("updateTime", getUpdateTime())
                .append("deleted", getDeleted())
                .toString();
    }
}
