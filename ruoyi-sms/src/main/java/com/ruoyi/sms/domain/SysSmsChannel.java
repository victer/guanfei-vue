package com.ruoyi.sms.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 短信渠道对象 sys_sms_channel
 *
 * @author ace
 * @date 2021-06-19
 */
@Data
public class SysSmsChannel extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 短信签名
     */
    @Excel(name = "短信签名")
    private String signature;

    /**
     * 渠道编码
     */
    @Excel(name = "渠道编码")
    private String code;

    /**
     * 开启状态
     */
    @Excel(name = "开启状态")
    private Integer status;

    /**
     * 短信 API 的账号
     */
    @Excel(name = "短信 API 的账号")
    private String apiKey;

    /**
     * 短信 API 的秘钥
     */
    @Excel(name = "短信 API 的秘钥")
    private String apiSecret;

    /**
     * 短信发送回调 URL
     */
    @Excel(name = "短信发送回调 URL")
    private String callbackUrl;

    /**
     * 创建者
     */
    @Excel(name = "创建者")
    private String creator;

    /**
     * 更新者
     */
    @Excel(name = "更新者")
    private String updater;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("signature", getSignature())
                .append("code", getCode())
                .append("status", getStatus())
                .append("remark", getRemark())
                .append("apiKey", getApiKey())
                .append("apiSecret", getApiSecret())
                .append("callbackUrl", getCallbackUrl())
                .append("creator", getCreator())
                .append("createTime", getCreateTime())
                .append("updater", getUpdater())
                .append("updateTime", getUpdateTime())
                .append("deleted", getDeleted())
                .toString();
    }
}
